﻿using System.Collections.Generic;
using System.Linq;
using iStone.Integration.GenericExport.Topics;

namespace iStone.Integration.GenericExport.FlattenedTopicExport.Tests
{
    public static class FlattenedTopicsExtensions
    {
        public static Topic GetFlatTopic(this FlattenedTopics flattenedTopics, string entityTypeId)
        {
            return flattenedTopics.FlattendedTopicsConfiguration.Topics
                .SingleOrDefault(topic => topic.Root.EntityType.Id == entityTypeId);
        }
        public static Topic GetOriginalSubTopic(this FlattenedTopics flattenedTopics, string entityTypeId)
        {
            return flattenedTopics.OriginalSubTopicsConfiguration.Topics
                .SingleOrDefault(topic => topic.Root.EntityType.Id == entityTypeId);
        }

        public static IEnumerable<string> GetNonRootEntityTypeIds(this FlattenedTopics flattenedTopics)
        {
            return flattenedTopics.NonRootEntityTypes
                .Select(entityType => entityType.Id)
                .ToList();
        }
    }
}
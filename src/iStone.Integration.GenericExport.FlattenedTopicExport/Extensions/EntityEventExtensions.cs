﻿using iStone.Integration.GenericExport.EntityEvents;

namespace iStone.Integration.GenericExport.FlattenedTopicExport.Extensions
{
    public static class EntityEventExtensions
    {
        public static bool IsChannelEvent(this EntityEvent entityEvent)
        {
            switch (entityEvent.EntityEventType)
            {
                case EntityEventType.ChannelEntityCreated:
                case EntityEventType.ChannelEntityUpdated:
                case EntityEventType.ChannelEntityDeleted:
                case EntityEventType.ChannelLinkCreated:
                case EntityEventType.ChannelLinkUpdated:
                case EntityEventType.ChannelLinkDeleted:
                    return true;
                default:
                    return false;
            }
        }

        public static bool IsEntityDeletedEvent(this EntityEvent entityEvent)
        {
            switch (entityEvent.EntityEventType)
            {
                case EntityEventType.EntityDeleted:
                case EntityEventType.ChannelLinkDeleted:
                    return true;
                default:
                    return false;
            }
        }

        public static bool IsEntityUpdatedEvent(this EntityEvent entityEvent)
        {
            switch (entityEvent.EntityEventType)
            {
                case EntityEventType.EntityUpdated:
                case EntityEventType.ChannelEntityUpdated:
                    return true;
                default:
                    return false;
            }
        }

        public static bool IsLinkEvent(this EntityEvent entityEvent)
        {
            switch (entityEvent.EntityEventType)
            {
                case EntityEventType.LinkCreated:
                case EntityEventType.LinkUpdated:
                case EntityEventType.LinkDeleted:
                case EntityEventType.ChannelLinkCreated:
                case EntityEventType.ChannelLinkUpdated:
                case EntityEventType.ChannelLinkDeleted:
                    return true;
                default:
                    return false;
            }
        }

        public static bool IsLinkCreatedEvent(this EntityEvent entityEvent)
        {
            switch (entityEvent.EntityEventType)
            {
                case EntityEventType.LinkCreated:
                case EntityEventType.ChannelLinkCreated:
                    return true;
                default:
                    return false;
            }
        }

        public static bool IsLinkDeletedEvent(this EntityEvent entityEvent)
        {
            switch (entityEvent.EntityEventType)
            {
                case EntityEventType.LinkDeleted:
                case EntityEventType.ChannelLinkDeleted:
                    return true;
                default:
                    return false;
            }
        }
    }
}

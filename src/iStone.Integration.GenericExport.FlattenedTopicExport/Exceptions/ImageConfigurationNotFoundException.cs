﻿using System;

namespace iStone.Integration.GenericExport.FlattenedTopicExport.Exceptions
{
    public class ImageConfigurationNotFoundException : Exception
    {
        public ImageConfigurationNotFoundException(string configurationName)
            : base($"Unable to find attribute '{configurationName}'.")
        {
            ConfigurationName = configurationName;
        }

        public string ConfigurationName { get; }
    }
}
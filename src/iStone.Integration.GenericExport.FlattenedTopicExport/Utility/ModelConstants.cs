﻿namespace iStone.Integration.GenericExport.FlattenedTopicExport.Utility
{
    public static class ModelConstants
    {
        public static class Resource
        {
            public const string EntityTypeId = "Resource";

            public const string FileId = "ResourceFileId";
            public const string Filename = "ResourceFilename";
        }

        public static class Channel
        {
            public const string EntityTypeId = "Channel";

            public const string Id = "ChannelId";
        }

        public static class ChannelNode
        {
            public const string EntityTypeId = "ChannelNode";
        }
    }
}

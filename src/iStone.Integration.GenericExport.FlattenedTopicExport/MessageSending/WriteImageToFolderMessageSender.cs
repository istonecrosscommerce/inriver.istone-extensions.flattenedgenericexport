﻿using System;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using iStone.inRiver.Core;
using iStone.inRiver.Portability.Interface;
using iStone.Integration.GenericExport.EntityEvents;
using iStone.Integration.GenericExport.FlattenedTopicExport.Extensions;
using iStone.Integration.GenericExport.FlattenedTopicExport.Utility;
using iStone.Integration.GenericExport.MessageSending;
using iStone.Integration.GenericExport.TopicMessages;

namespace iStone.Integration.GenericExport.FlattenedTopicExport.MessageSending
{
    public class WriteImageToFolderMessageSender : ISimpleMessageSender<(TopicMessage Message, XElement Xml), FileInfo>
    {
        private readonly IReadOnlyPimApi _pimApi;
        private readonly ILogger _logger;
        private readonly string _displayConfiguration;
        private readonly DirectoryInfo _outputDirectory;
        private readonly bool _createMissingDirectories;

        public WriteImageToFolderMessageSender(IPimApi pimApi, string resourceConfiguration,
            DirectoryInfo outputDirectory, bool createMissingDirectories)
        {
            _pimApi = pimApi ?? throw new ArgumentNullException(nameof(pimApi));
            _logger = pimApi.Logger;
            _displayConfiguration = resourceConfiguration ?? throw new ArgumentNullException(nameof(resourceConfiguration));
            _outputDirectory = outputDirectory ?? throw new ArgumentNullException(nameof(outputDirectory));
            _createMissingDirectories = createMissingDirectories;
        }

        public FileInfo SendMessage((TopicMessage Message, XElement Xml) topicMessageTuple)
        {
            if (topicMessageTuple.Message.EntityEvents.Count != 1)
            {
                throw new Exception("This Message Sender can't handle Topic Messages with multiple Entity Events.");
            }

            var topicMessage = topicMessageTuple.Message;
            if (!IsRelevant(topicMessage))
            {
                _logger.Verbose($"Topic message for {topicMessage.RootEntity.EntityType.Id} {topicMessage.RootEntity.Id} should not result in an image being written, ignoring: ");
                return null;
            }
            
            return WriteResourceFile(topicMessage.RootEntity);
        }

        private static bool IsRelevant(TopicMessage topicMessage)
        {
            var entityEvent = topicMessage.EntityEvents.Single();
            return IsResourceLinkedEvent(topicMessage, entityEvent)
                   || IsResourceFileIdUpdatedEvent(entityEvent);
        }

        private static bool IsResourceLinkedEvent(TopicMessage topicMessage, EntityEvent entityEvent)
        {
            return entityEvent.IsLinkEvent() &&
                   topicMessage.RootEntity.EntityType.Id == ModelConstants.Resource.EntityTypeId;
        }

        private static bool IsResourceFileIdUpdatedEvent(EntityEvent entityEvent)
        {
            return entityEvent.EntityEventType == EntityEventType.EntityUpdated
                   && entityEvent.EntityCreatedOrUpdatedInfo.FieldTypeIds.Contains(ModelConstants.Resource.FileId);
        }

        private FileInfo WriteResourceFile(EntityView resourceView)
        {
            if (resourceView.EntityType.Id != ModelConstants.Resource.EntityTypeId)
            {
                throw new Exception($"Expected a {ModelConstants.Resource.EntityTypeId} EntityView, got {resourceView.EntityType.Id}.");
            }

            var resourceFileIdFieldView = resourceView.GetFieldView(ModelConstants.Resource.FileId);
            var resourceFileId = (int?)resourceFileIdFieldView.Value;
            if (resourceFileId == null)
            {
                _logger.Information($"{ModelConstants.Resource.FileId} is empty for {ModelConstants.Resource.EntityTypeId} {resourceView.Id}, can't write image file.");
                return null;
            }

            var outputPath = GetOutputPath(resourceView);

            WriteResourceFile(resourceFileId.Value, outputPath);

            return new FileInfo(outputPath);
        }

        private string GetOutputPath(EntityView resourceView)
        {
            var fileNameFieldView = resourceView.GetFieldView(ModelConstants.Resource.Filename);
            var fileName = (string)fileNameFieldView.Value;

            var extension = Path.GetExtension(fileName);
            var outputPath = Path.Combine(_outputDirectory.FullName, $"{resourceView.Id}{extension}");
            return outputPath;
        }

        private void WriteResourceFile(int resourceFileId, string outputPath)
        {
            // Workaround for bug in PimUtilityService, the null check should be enough
            byte[] imageBytes;
            try
            {
                imageBytes = _pimApi.UtilityGetter.GetFile(resourceFileId, _displayConfiguration);
            }
            catch (ArgumentNullException)
            {
                imageBytes = null;
            }

            if (imageBytes == null)
            {
                throw new Exception($"No image file found with {ModelConstants.Resource.FileId} {resourceFileId}, can't write image file.");
            }

            if (!Directory.Exists(_outputDirectory.FullName))
            {
                CreateMissingOutputDirectory(_outputDirectory.FullName);
            }

            File.WriteAllBytes(outputPath, imageBytes);
        }

        private void CreateMissingOutputDirectory(string outputPath)
        {
            if (!_createMissingDirectories)
            {
                throw new Exception($"Output directory missing: {outputPath}");
            }

            try
            {
                Directory.CreateDirectory(outputPath);
            }
            catch (Exception e)
            {
                throw new Exception(
                    $"Could not create missing output directory: {outputPath}",
                    e);
            }
        }
    }
}

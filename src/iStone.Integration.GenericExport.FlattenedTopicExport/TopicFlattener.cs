﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using iStone.inRiver.Core;
using iStone.inRiver.Core.Comparers;
using iStone.inRiver.Portability.Objects;
using iStone.inRiver.Portability.Query;
using iStone.Integration.GenericExport.Criteria;
using iStone.Integration.GenericExport.EntityEvents;
using iStone.Integration.GenericExport.FieldTemplates;
using iStone.Integration.GenericExport.FlattenedTopicExport.Extensions;
using iStone.Integration.GenericExport.FlattenedTopicExport.Utility;
using iStone.Integration.GenericExport.Topics;

namespace iStone.Integration.GenericExport.FlattenedTopicExport
{
    public class FlattenedTopics
    {
        public FlattenedTopics(TopicConfiguration flatTopicsConfiguration,
            TopicConfiguration originalSubTopicsConfiguration, IReadOnlyCollection<EntityType> nonRootEntityTypes)
        {
            FlattendedTopicsConfiguration = flatTopicsConfiguration ?? throw new ArgumentNullException(nameof(flatTopicsConfiguration));
            OriginalSubTopicsConfiguration = originalSubTopicsConfiguration ?? throw new ArgumentNullException(nameof(originalSubTopicsConfiguration));
            NonRootEntityTypes = nonRootEntityTypes?.ToList() ?? throw new ArgumentNullException(nameof(nonRootEntityTypes));
        }

        public TopicConfiguration FlattendedTopicsConfiguration { get; }
        public TopicConfiguration OriginalSubTopicsConfiguration { get; }
        public IReadOnlyCollection<EntityType> NonRootEntityTypes { get; }
    }

    public class TopicFlattener
    {
        private static readonly EntityTypeIdComparer EntityTypeIdComparer = new EntityTypeIdComparer();
        private static readonly FieldTypeIdComparer FieldTypeIdComparer = new FieldTypeIdComparer();

        private readonly IPimApi _pimApi;
        private readonly CriteriaFactory _criteriaFactory;
        private readonly ISet<EntityType> _nonRootEntityTypes;
        private readonly ICriteria _rootCriteria;
        private readonly CultureInfo _topicNameDescriptionLanguage;

        public TopicFlattener(IPimApi pimApi, CriteriaFactory criteriaFactory, IEnumerable<EntityType> nonRootEntityTypes,
            ICriteria rootCriteria, CultureInfo topicNameDescriptionLanguage)
        {
            _pimApi = pimApi ?? throw new ArgumentNullException(nameof(pimApi));
            _rootCriteria = rootCriteria ?? throw new ArgumentNullException(nameof(rootCriteria));
            _criteriaFactory = criteriaFactory ?? throw new ArgumentNullException(nameof(criteriaFactory));
            _topicNameDescriptionLanguage = topicNameDescriptionLanguage ??
                                            throw new ArgumentNullException(nameof(topicNameDescriptionLanguage));

            _nonRootEntityTypes = new HashSet<EntityType>(
                nonRootEntityTypes ?? throw new ArgumentNullException(nameof(nonRootEntityTypes)),
                EntityTypeIdComparer);
        }

        public FlattenedTopics FlattenTopic(Topic originalTopic, IReadOnlyCollection<EntityType> blockedEntityTypes,
            IReadOnlyCollection<FieldType> blockedFieldTypes, IReadOnlyCollection<LinkType> blockedLinkTypes)
        {
            if (originalTopic == null) throw new ArgumentNullException(nameof(originalTopic));

            var originalRoot = originalTopic.Root;

            ValidateNode(originalRoot, new EntityType[0]);

            var chains = originalRoot.GetGraphNodeLinkChains();
            var flattenedCriterias = CreateCriteriaPerEntityType(originalRoot.EntityType, chains);
            var alwaysTrueCriterias = flattenedCriterias
                .ToDictionary(kvp => kvp.Key, kvp => _criteriaFactory.CreateAlwaysTrueCriteria(), flattenedCriterias.Comparer);

            var flattenedTopics = CreateFlattenedTopics(originalTopic, originalRoot, flattenedCriterias, alwaysTrueCriterias);
            var originalSubTopics = CreateOriginalSubTopics(originalTopic, originalRoot, flattenedCriterias, alwaysTrueCriterias);

            TopicConfiguration CreateTopicConfiguration(IEnumerable<Topic> topics) =>
                new TopicConfiguration(topics, blockedEntityTypes, blockedFieldTypes, blockedLinkTypes);

            return new FlattenedTopics(
                CreateTopicConfiguration(flattenedTopics),
                CreateTopicConfiguration(originalSubTopics),
                _nonRootEntityTypes.ToList());
        }

        private List<Topic> CreateFlattenedTopics(Topic originalTopic, EntityTypeGraphNode originalRoot,
            IReadOnlyDictionary<EntityType, ICriteria> flattenedCriterias,
            IReadOnlyDictionary<EntityType, ICriteria> alwaysTrueCriterias)
        {
            return GetAllNodes(originalRoot)
                .Where(node => !_nonRootEntityTypes.Contains(node.EntityType))
                .Where(node => !node.EntityType.IsLinkEntityType)
                .Select(node => CopyNode(node, alwaysTrueCriterias, 1))
                .GroupBy(node => node.EntityType, EntityTypeIdComparer)
                .Select(node => MergeNodes(node, flattenedCriterias))
                .Select(newRoot => CreateTopic(newRoot, originalTopic))
                .ToList();
        }

        private List<Topic> CreateOriginalSubTopics(Topic originalTopic, EntityTypeGraphNode originalRoot,
            IReadOnlyDictionary<EntityType, ICriteria> flattenedCriterias,
            IReadOnlyDictionary<EntityType, ICriteria> alwaysTrueCriterias)
        {
            return GetAllNodes(originalRoot)
                .Where(node => !_nonRootEntityTypes.Contains(node.EntityType))
                .Where(node => !node.EntityType.IsLinkEntityType)
                .Select(node => CopyNode(node, alwaysTrueCriterias, default(int?)))
                .GroupBy(node => node.EntityType, EntityTypeIdComparer)
                .Select(node => MergeNodes(node, flattenedCriterias))
                .Select(newRoot => CreateTopic(newRoot, originalTopic))
                .ToList();
        }

        private void ValidateNode(EntityTypeGraphNode node, IReadOnlyCollection<EntityType> encounteredEntityTypes)
        {
            var entityType = node.EntityType;
            if (!_nonRootEntityTypes.Contains(entityType, EntityTypeIdComparer) &&
                encounteredEntityTypes.Contains(entityType, EntityTypeIdComparer) &&
                encounteredEntityTypes.Last().Id == entityType.Id)
            {
                // Is this needed?
                throw new Exception("Cyclic entity graphs are not supported.");
            }

            if (node.FieldViewTemplates.Any())
            {
                // FieldViewTemplates should be mergeable based on Id?
                throw new Exception("Field View Templates are not supported.");
            }

            foreach (var child in node.Children)
            {
                if (child.LinkEntityGraphNode != null && child.LinkEntityGraphNode.Children.Any())
                {
                    throw new Exception("Links to or from LinkEntities are not supported.");
                }

                ValidateNode(child.GraphNode, encounteredEntityTypes.Append(entityType).ToList());
            }
        }

        private Dictionary<EntityType, ICriteria> CreateCriteriaPerEntityType(
            EntityType rootEntityType,
            IReadOnlyDictionary<EntityType, List<EntityTypeGraphNodeLinkChain>> chains)
        {
            var criterias = new Dictionary<EntityType, ICriteria>(EntityTypeIdComparer)
            {
                {rootEntityType, _rootCriteria}
            };

            foreach (var entityType in chains.Keys)
            {
                if (!criterias.ContainsKey(entityType))
                {
                    criterias[entityType] = entityType.IsLinkEntityType
                        ? _criteriaFactory.CreateAlwaysTrueCriteria()
                        : GetCriteriaForEntityType(entityType, chains, criterias);
                }
            }

            return criterias;
        }

        private ICriteria GetCriteriaForEntityType(EntityType entityType,
            IReadOnlyDictionary<EntityType, List<EntityTypeGraphNodeLinkChain>> chains,
            IDictionary<EntityType, ICriteria> criterias)
        {
            if (criterias.ContainsKey(entityType))
            {
                return criterias[entityType];
            }

            var sourceCriterias = chains[entityType]
                .Select(chain => new LinkTypeWithDirection(chain.LinkType, chain.LinkDirection))
                .GroupBy(linKTypeWithDirection => linKTypeWithDirection.SourceEntityTypeId)
                .Select(linkTypesWithDirection => new LinkCardinalityCriteria(
                    CreateLinkTypeWithReversedDirectionTuples(linkTypesWithDirection), 1, null,
                    GetCriteriaForEntityType(_pimApi.ModelGetter.GetEntityType(linkTypesWithDirection.Key), chains, criterias),
                    _criteriaFactory.CreateAlwaysTrueCriteria(), true, _pimApi))
                .ToList();

            var criteria = sourceCriterias.Count > 1
                ? _criteriaFactory.CreateCompoundCriteria(sourceCriterias, Join.Or)
                : sourceCriterias.Single();

            return criterias[entityType] = criteria;
        }

        private static IEnumerable<Tuple<LinkType, LinkDirection>> CreateLinkTypeWithReversedDirectionTuples(
            IEnumerable<LinkTypeWithDirection> linkTypesWithDirection)
        {
            return linkTypesWithDirection
                .Select(linkTypeWithDirection => Tuple.Create(
                    linkTypeWithDirection.LinkType,
                    InvertLinkDirection(linkTypeWithDirection.LinkDirection)));
        }

        private static LinkDirection InvertLinkDirection(LinkDirection linkDirection)
        {
            return linkDirection == LinkDirection.OutBound
                ? LinkDirection.InBound
                : LinkDirection.OutBound;
        }

        private static IEnumerable<EntityTypeGraphNode> GetAllNodes(EntityTypeGraphNode node)
        {
            return node.Children
                .SelectMany(child => GetAllNodes(child.GraphNode))
                .Prepend(node);
        }

        private EntityTypeGraphNode CopyNode(EntityTypeGraphNode node,
            IReadOnlyDictionary<EntityType, ICriteria> criterias, int? maxDepth)
        {
            var fieldTypes = maxDepth != 0 || node.EntityType.IsLinkEntityType
                ? node.FieldTypes
                : Enumerable.Empty<FieldType>();

            var children = maxDepth != 0
                ? node.Children.Select(link => CopyChildNodeLink(link, criterias, maxDepth - 1))
                : Enumerable.Empty<EntityTypeGraphNodeLink>();

            return new EntityTypeGraphNode(node.EntityType, criterias[node.EntityType], fieldTypes,
                Enumerable.Empty<IFieldViewTemplate>(), children);
        }

        private EntityTypeGraphNodeLink CopyChildNodeLink(EntityTypeGraphNodeLink link,
            IReadOnlyDictionary<EntityType, ICriteria> criterias, int? maxDepth)
        {
            return new EntityTypeGraphNodeLink(
                CopyNode(link.GraphNode, criterias, maxDepth),
                link.LinkType, link.LinkDirection,
                CopyLinkEntityNode(link.LinkEntityGraphNode, criterias));
        }

        private EntityTypeGraphNode CopyLinkEntityNode(EntityTypeGraphNode node,
            IReadOnlyDictionary<EntityType, ICriteria> criterias)
        {
            return node != null
                ? CopyNode(node, criterias, 0)
                : null;
        }

        private EntityTypeGraphNode MergeNodes(IGrouping<EntityType, EntityTypeGraphNode> nodes,
            IReadOnlyDictionary<EntityType, ICriteria> criterias)
        {
            return MergeNodes(nodes.Key, nodes.ToList(), criterias);
        }

        private EntityTypeGraphNode MergeNodes(EntityType entityType,
            IReadOnlyCollection<EntityTypeGraphNode> nodes,
            IReadOnlyDictionary<EntityType, ICriteria> criterias)
        {
            var fieldTypes = nodes
                .SelectMany(node => node.FieldTypes)
                .Distinct(FieldTypeIdComparer);

            var children = nodes
                .SelectMany(node => node.Children)
                .GroupBy(child => new LinkTypeWithDirection(child.LinkType, child.LinkDirection))
                .Select(childNodes => MergeNodeLinks(childNodes, criterias));

            return new EntityTypeGraphNode(entityType, criterias[entityType], fieldTypes,
                Enumerable.Empty<IFieldViewTemplate>(), children);
        }

        private EntityTypeGraphNodeLink MergeNodeLinks(
            IGrouping<LinkTypeWithDirection, EntityTypeGraphNodeLink> children,
            IReadOnlyDictionary<EntityType, ICriteria> criterias)
        {
            return MergeNodeLinks(children.Key.LinkType, children.Key.LinkDirection, children.ToList(), criterias);
        }

        private EntityTypeGraphNodeLink MergeNodeLinks(LinkType linkType, LinkDirection linkDirection,
            IReadOnlyCollection<EntityTypeGraphNodeLink> children, IReadOnlyDictionary<EntityType, ICriteria> criterias)
        {
            var childNodes = children
                .Select(link => link.GraphNode)
                .GroupBy(node => node.EntityType, EntityTypeIdComparer)
                .Single();

            var linkEntityNodes = children
                .Select(child => child.LinkEntityGraphNode)
                .Where(node => node != null)
                .GroupBy(node => node.EntityType, EntityTypeIdComparer)
                .SingleOrDefault();

            return new EntityTypeGraphNodeLink(
                MergeNodes(childNodes, criterias), linkType, linkDirection,
                linkEntityNodes != null ? MergeNodes(linkEntityNodes, criterias) : null);
        }

        private Topic CreateTopic(EntityTypeGraphNode root, Topic originalTopic)
        {
            return new Topic(
                $"{originalTopic.Name} ({root.EntityType.Name[_topicNameDescriptionLanguage]} flat root)",
                $"{originalTopic.Description} ({root.EntityType.Name[_topicNameDescriptionLanguage]} flat root)",
                originalTopic.Metadata, root, originalTopic.EntityEventTypes,
                CreateDictionaryOfEnumerableValues(originalTopic.EntityTypeTriggers),
                CreateDictionaryOfEnumerableValues(originalTopic.FieldTypeTriggers),
                CreateDictionaryOfEnumerableValues(originalTopic.LinkTypeTriggers));
        }

        public static Dictionary<EntityEventType, IEnumerable<T>> CreateDictionaryOfEnumerableValues<T>(
            IReadOnlyDictionary<EntityEventType, IReadOnlyList<T>> source)
        {
            return source.ToDictionary(kvp => kvp.Key, kvp => kvp.Value.AsEnumerable());
        }
    }
}

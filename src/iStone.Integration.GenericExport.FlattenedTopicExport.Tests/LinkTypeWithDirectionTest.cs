﻿using System.Collections.Generic;
using System.Linq;
using iStone.inRiver.Portability.Objects;
using iStone.inRiver.Portability.Query;
using iStone.Integration.GenericExport.FlattenedTopicExport.Utility;
using NUnit.Framework;

namespace iStone.Integration.GenericExport.FlattenedTopicExport.Tests
{
    [TestFixture]
    public class LinkTypeWithDirectionTest
    {
        private const string ChannelChannelNodeLinkTypeId = "ChannelChannelNode";
        private static readonly LinkType _channelChannelNode = new LinkType
        {
            Id = ChannelChannelNodeLinkTypeId,
            Index = 0,
            SourceEntityTypeId = "Channel",
            TargetEntityTypeId = "ChannelNode",
        };

        private const string ChannelNodeChannelNodeLinkTypeId = "ChannelNodeChannelNode";
        private static readonly LinkType _channelNodeChannelNode = new LinkType
        {
            Id = ChannelNodeChannelNodeLinkTypeId,
            Index = 1,
            SourceEntityTypeId = "ChannelNode",
            TargetEntityTypeId = "ChannelNode",
        };

        private static readonly Dictionary<string, LinkType> LinkTypes =
            new[] {_channelChannelNode, _channelNodeChannelNode}.ToDictionary(linkType => linkType.Id);


        [TestCase(ChannelChannelNodeLinkTypeId, LinkDirection.OutBound, ChannelChannelNodeLinkTypeId, LinkDirection.OutBound, TestName = "LinkTypeWithDirections with same LinkType and Direction compare equal")]
        public void Equality(string linkTypeId1, LinkDirection direction1,
                             string linkTypeId2, LinkDirection direction2)
        {
            var lhs = new LinkTypeWithDirection(LinkTypes[linkTypeId1], direction1);
            var rhs = new LinkTypeWithDirection(LinkTypes[linkTypeId2], direction2);

            Assert.AreEqual(lhs, rhs);

            Assert.AreEqual(lhs.GetHashCode(), rhs.GetHashCode());
            Assert.IsTrue(lhs.Equals((object)rhs));
        }

        [TestCase(ChannelChannelNodeLinkTypeId, LinkDirection.OutBound, ChannelChannelNodeLinkTypeId, LinkDirection.InBound, TestName = "LinkTypeWithDirections with different Direction compare unequal")]
        [TestCase(ChannelChannelNodeLinkTypeId, LinkDirection.OutBound, ChannelNodeChannelNodeLinkTypeId, LinkDirection.OutBound, TestName = "LinkTypeWithDirections with different LinkType compare unequal")]
        [TestCase(ChannelChannelNodeLinkTypeId, LinkDirection.OutBound, ChannelNodeChannelNodeLinkTypeId, LinkDirection.InBound, TestName = "LinkTypeWithDirections with different LinkType and Direction compare unequal")]
        public void Inequality(string linkTypeId1, LinkDirection direction1,
            string linkTypeId2, LinkDirection direction2)
        {
            var lhs = new LinkTypeWithDirection(LinkTypes[linkTypeId1], direction1);
            var rhs = new LinkTypeWithDirection(LinkTypes[linkTypeId2], direction2);

            Assert.AreNotEqual(lhs, rhs);
            Assert.AreNotEqual(lhs, null);
            Assert.AreNotEqual(rhs, null);

            Assert.IsFalse(lhs.Equals((object)rhs));
        }

        [TestCase(ChannelChannelNodeLinkTypeId, LinkDirection.OutBound, ChannelChannelNodeLinkTypeId, LinkDirection.OutBound, TestName = "LinkTypeWithDirections with same LinkType and Direction order consistently")]
        [TestCase(ChannelChannelNodeLinkTypeId, LinkDirection.OutBound, ChannelChannelNodeLinkTypeId, LinkDirection.InBound, TestName = "LinkTypeWithDirections with different Direction order consistently")]
        [TestCase(ChannelChannelNodeLinkTypeId, LinkDirection.OutBound, ChannelNodeChannelNodeLinkTypeId, LinkDirection.OutBound, TestName = "LinkTypeWithDirections with different LinkType order consistently")]
        [TestCase(ChannelChannelNodeLinkTypeId, LinkDirection.OutBound, ChannelNodeChannelNodeLinkTypeId, LinkDirection.InBound, TestName = "LinkTypeWithDirections with different LinkType and Direction order consistently")]
        public void Ordering(string linkTypeId1, LinkDirection direction1,
            string linkTypeId2, LinkDirection direction2)
        {
            var lhs = new LinkTypeWithDirection(LinkTypes[linkTypeId1], direction1);
            var rhs = new LinkTypeWithDirection(LinkTypes[linkTypeId2], direction2);

            var lhsDiff = lhs.CompareTo(rhs);
            var rhsDiff = rhs.CompareTo(lhs);

            if (lhsDiff == 0)
            {
                Assert.AreEqual(rhsDiff, 0);
                Assert.AreEqual(lhs, rhs);
            }
            else
            {
                Assert.AreNotEqual(rhsDiff, 0);
                Assert.AreNotEqual(lhsDiff < 0, rhsDiff < 0);
            }
        }
    }
}

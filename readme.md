FlattenedTopicExort

An outbound connector which exports entity xml files on updates. The files are exported to one directory per entity type. Images are also exported, and put in a subdirectory of the Resource export directory.

It uses the standard GenericExport topic xml configuration file, and the default GenericExport output xml format, but the topic is interpreted differently than in standard GenericExport.

Topic Transformation

Example
<Product>
	<Item>
		<Resource/>
	</Item>
	<Resource/>
</Product>

In standard GenericExport, the preceding simplified topic structure would result in an export with Product as root for each relevant update, with multiple levels of entities in the same file. In this connector, it would instead result in separate exports for multiple entities, as if there were multiple topics as shown below, with criteria to ensure that Items and Resources will only be exported if they are linked to a Product, or an Item linked to a Product, respectively. Only the root entity (and any link entities) will have their fields exported in each file.

<Product>
	<Item/>
	<Resource/>
</Product>

<Item>
	<Resource/>
</Item>

<Resource/>

Field updates result in only that entity being exported.
Entity delete and Link delete result in the source (as configured in the topic, not necessarily as seen in inRiver) being exported.
Link added result in both the source, target and all entities linked from the target in the topic, recursively, being exported.

Limitations
Any criterias in the original topic will be silently ignored. FieldTemplates in the topic file will result in an configuration error.

Connector settings

TOPIC_CONFIGURATION_FILE - The GenericExport topic configuration to be used
TOPIC_NAME_DESCRIPTION_LANGUAGE - The language to use for naming the flattened topics generated from the original topic (the new names contain the EntityType name in this language) Defaults to the master language
NON_ROOT_ENTITY_TYPES - Entity types which will not be exported in files of their own, but which will be exported as links
OUTPUT_BASE_DIRECTORY - The base directory for exports, the final files will be created in subdirectories of this directory named after the system names of each exported entity type
IMAGE_OUTPUT_DIRECTORY - The directory to export images to, relative to the base directory
RESOURCE_CONFIGURATION - The resource configuration used when exporting images
CREATE_MISSING_DIRECTORIES - True/False, if true missing entity and image directories will be created when a relevant file is exported, if false a missing directory results in an error
CHANNEL_ID - Entity id of the Channel which all entities must be included in to be exported
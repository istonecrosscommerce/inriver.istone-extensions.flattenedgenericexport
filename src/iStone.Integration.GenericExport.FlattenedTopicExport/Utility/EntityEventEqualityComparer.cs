﻿using System;
using System.Collections.Generic;
using iStone.Integration.GenericExport.EntityEvents;

namespace iStone.Integration.GenericExport.FlattenedTopicExport.Utility
{
    // IEqualityComparer which ignores Id (Guid) and OccuredAt
    public class EntityEventEqualityComparer : IEqualityComparer<EntityEvent>
    {
        // Workaround for these types not being comparable in GenericExport
        private static readonly IEqualityComparer<RegisteredEntityInfo> RegisteredEntityInfoComparer =
            new ToStringEqualityComparer<RegisteredEntityInfo>();
        private static readonly IEqualityComparer<EntityEventInfo> EntityEventInfoComparer =
            new ToStringEqualityComparer<EntityEventInfo>();
        private static readonly IEqualityComparer<EntityDeletedInfo> EntityDeletedInfoComparer =
            new ToStringEqualityComparer<EntityDeletedInfo>();
        private static readonly IEqualityComparer<LinkEventInfo> LinkEventInfoComparer =
            new ToStringEqualityComparer<LinkEventInfo>();

        public bool Equals(EntityEvent x, EntityEvent y)
        {
            if (x == null)
            {
                return y == null;
            }
            if (y == null)
            {
                return false;
            }

            if (x.EntityEventType != y.EntityEventType ||
                x.ChannelId != y.ChannelId ||
                x.ConnectorId != y.ConnectorId)
            {
                return false;
            }

            switch (x.EntityEventType)
            {
                case EntityEventType.RegisteredEntityCreated:
                case EntityEventType.RegisteredEntityResent:
                    return RegisteredEntityInfoComparer.Equals(x.RegisteredEntityInfo, y.RegisteredEntityInfo);
                case EntityEventType.EntityCreated:
                case EntityEventType.EntityUpdated:
                case EntityEventType.ChannelEntityCreated:
                case EntityEventType.ChannelEntityUpdated:
                    return EntityEventInfoComparer.Equals(x.EntityCreatedOrUpdatedInfo, y.EntityCreatedOrUpdatedInfo);
                case EntityEventType.EntityDeleted:
                case EntityEventType.ChannelEntityDeleted:
                    return EntityDeletedInfoComparer.Equals(x.EntityDeletedInfo, y.EntityDeletedInfo);
                case EntityEventType.LinkCreated:
                case EntityEventType.LinkUpdated:
                case EntityEventType.LinkDeleted:
                case EntityEventType.ChannelLinkCreated:
                case EntityEventType.ChannelLinkUpdated:
                case EntityEventType.ChannelLinkDeleted:
                    return LinkEventInfoComparer.Equals(x.LinkEventInfo, y.LinkEventInfo);
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public int GetHashCode(EntityEvent obj)
        {
            switch (obj.EntityEventType)
            {
                case EntityEventType.RegisteredEntityCreated:
                case EntityEventType.RegisteredEntityResent:
                    return RegisteredEntityInfoComparer.GetHashCode(obj.RegisteredEntityInfo);
                case EntityEventType.EntityCreated:
                case EntityEventType.EntityUpdated:
                case EntityEventType.ChannelEntityCreated:
                case EntityEventType.ChannelEntityUpdated:
                    return EntityEventInfoComparer.GetHashCode(obj.EntityCreatedOrUpdatedInfo);
                case EntityEventType.EntityDeleted:
                case EntityEventType.ChannelEntityDeleted:
                    return EntityDeletedInfoComparer.GetHashCode(obj.EntityDeletedInfo);
                case EntityEventType.LinkCreated:
                case EntityEventType.LinkUpdated:
                case EntityEventType.LinkDeleted:
                case EntityEventType.ChannelLinkCreated:
                case EntityEventType.ChannelLinkUpdated:
                case EntityEventType.ChannelLinkDeleted:
                    return LinkEventInfoComparer.GetHashCode(obj.LinkEventInfo);
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public static EntityEventEqualityComparer Instance { get; } = new EntityEventEqualityComparer();
    }
}

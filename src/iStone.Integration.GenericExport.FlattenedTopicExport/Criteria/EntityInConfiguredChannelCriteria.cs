﻿using System;
using System.Collections.Generic;
using System.Linq;
using iStone.inRiver.Core;
using iStone.inRiver.Portability.Objects;
using iStone.Integration.GenericExport.Criteria;
using PimCache = iStone.Integration.GenericExport.Utilities.PimCache;

namespace iStone.Integration.GenericExport.FlattenedTopicExport.Criteria
{
    public class EntityInConfiguredChannelCriteria : CacheableCriteria, ICustomCriteria
    {
        private readonly int _channelId;
        private readonly IPimApi _pimApi;

        public override bool TriggerCriteria => false;
        public override IReadOnlyCollection<FieldType> FieldTypes { get; } = new FieldType[0];
        public override IReadOnlyCollection<LinkType> LinkTypes { get; } = new LinkType[0];
        public string ConfigId { get; } = "EntityInConfiguredChannel";

        public EntityInConfiguredChannelCriteria(int channelId, IPimApi pimApi)
        {
            _channelId = channelId;
            _pimApi = pimApi ?? throw new ArgumentNullException(nameof(pimApi));
        }

        protected override bool EntityIsValidInternal(int entityId, EvaluationInfo evaluationInfo,
            PimCache pimCache, EvaluationCache evaluationCache = null)
        {
            return _pimApi.ChannelService.EntityExistsInChannel(_channelId, entityId)
                || LinkedSourceEntityExistsInChannel(entityId);
        }

        private bool LinkedSourceEntityExistsInChannel(int entityId)
        {
            /*
             * Check for the parents as well, since the channelstructure on
             * the server can sometimes lag behind a little bit.
             */
            return _pimApi.DataGetter.GetInboundLinksForEntity(entityId)
                .Any(link => _pimApi.ChannelService.EntityExistsInChannel(_channelId, link.Source.Id));
        }
    }
}
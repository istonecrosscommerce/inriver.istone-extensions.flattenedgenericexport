﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using iStone.inRiver.Portability.Interface;
using iStone.Integration.GenericExport.EntityEvents;
using iStone.Integration.GenericExport.MessageSending;

namespace iStone.Integration.GenericExport.FlattenedTopicExport.MessageSending
{
    public class LogFileSavedMessageSentResultHandler : IMessageSentResultHandler<IReadOnlyList<FileInfo>>
    {
        private readonly ILogger _logger;

        public LogFileSavedMessageSentResultHandler(ILogger logger)
        {
            if (logger != null) _logger = logger;
        }

        public void HandleResults(EntityEvent entityEvent, IReadOnlyList<FileInfo>[] results)
        {
            _logger.Debug($"EntityEvent was succesfully handled: {entityEvent}");
            foreach (var fileInfo in results.SelectMany(_ => _))
            {
                _logger.Information($"Saved output file: {fileInfo.FullName}");
            }
        }

        public void HandleError(EntityEvent entityEvent, Exception e)
        {
            _logger.Error($"EntityEvent was not succesfully handled: {entityEvent}{Environment.NewLine}{e}");
        }
    }
}

﻿using System;
using System.Collections.Generic;

namespace iStone.Integration.GenericExport.FlattenedTopicExport.Extensions
{
    public static class EnumerableExtensions
    {
        public static IEnumerable<TSource> DistinctBy<TSource, TKey>(
            this IEnumerable<TSource> source, Func<TSource, TKey> keySelector)
        {
            return DistinctBy(source, keySelector, EqualityComparer<TKey>.Default);
        }

        public static IEnumerable<TSource> DistinctBy<TSource, TKey>(
            this IEnumerable<TSource> source, Func<TSource, TKey> keySelector,
            IEqualityComparer<TKey> comparer)
        {
            if (source == null) throw new ArgumentNullException(nameof(source));
            if (keySelector == null) throw new ArgumentNullException(nameof(keySelector));
            if (comparer == null) throw new ArgumentNullException(nameof(comparer));

            var seenKeys = new HashSet<TKey>(comparer);
            foreach (var element in source)
            {
                if (seenKeys.Add(keySelector(element)))
                {
                    yield return element;
                }
            }
        }

        public static IEnumerable<TSource> Prepend<TSource>(
            this IEnumerable<TSource> source, TSource newElement)
        {
            if (source == null) throw new ArgumentNullException(nameof(source));

            yield return newElement;

            foreach (var element in source)
            {
                yield return element;
            }
        }

        public static IEnumerable<TSource> Append<TSource>(
            this IEnumerable<TSource> source, TSource newElement)
        {
            if (source == null) throw new ArgumentNullException(nameof(source));

            foreach (var element in source)
            {
                yield return element;
            }

            yield return newElement;
        }
    }
}

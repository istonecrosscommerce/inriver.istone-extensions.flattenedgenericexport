﻿using System.Collections.Generic;
using iStone.Integration.GenericExport.Criteria;
using iStone.Integration.GenericExport.EntityEvents;
using iStone.Integration.GenericExport.TopicMessages;
using iStone.Integration.GenericExport.Utilities;

namespace iStone.Integration.GenericExport.FlattenedTopicExport
{
    public interface ITopicMessagesCreator
    {
        List<TopicMessage> CreateTopicMessages(EntityEvent entityEvent, EvaluationInfo evaluationInfo, PimCache pimCache);
    }
}
﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using iStone.Integration.GenericExport.MessageSending;
using iStone.Integration.GenericExport.TopicMessages;

namespace iStone.Integration.GenericExport.FlattenedTopicExport.MessageSending
{
    public class WriteFilesMessageSender : ISimpleMessageSender<ValueTuple<TopicMessage, XElement>, List<FileInfo>>
    {
        private readonly IReadOnlyList<ISimpleMessageSender<ValueTuple<TopicMessage, XElement>, FileInfo>> _messageSenders;

        public WriteFilesMessageSender(
            IEnumerable<ISimpleMessageSender<ValueTuple<TopicMessage, XElement>, FileInfo>> messageSenders)
        {
            _messageSenders = messageSenders?.ToList() ?? throw new ArgumentNullException(nameof(messageSenders));
        }

        public List<FileInfo> SendMessage(ValueTuple<TopicMessage, XElement> topicMessage)
        {
            return _messageSenders
                .Select( messageSender => messageSender.SendMessage(topicMessage))
                .Where(fileInfo => fileInfo != null)
                .ToList();
        }
    }
}

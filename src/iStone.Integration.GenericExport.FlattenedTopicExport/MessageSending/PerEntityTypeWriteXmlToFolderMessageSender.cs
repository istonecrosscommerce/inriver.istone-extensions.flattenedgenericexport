﻿using System;
using System.IO;
using System.Xml.Linq;
using iStone.inRiver.Portability.Objects;
using iStone.Integration.GenericExport.MessageSending;
using iStone.Integration.GenericExport.TopicMessages;

namespace iStone.Integration.GenericExport.FlattenedTopicExport.MessageSending
{
    public class PerEntityTypeWriteXmlToFolderMessageSender : ISimpleMessageSender<(TopicMessage Message, XElement Xml), FileInfo>
    {
        private readonly DirectoryInfo _baseDirectory;
        private readonly bool _createMissingDirectories;

        public PerEntityTypeWriteXmlToFolderMessageSender(DirectoryInfo baseDirectory,
            bool createMissingDirectories)
        {
            _baseDirectory = baseDirectory ?? throw new ArgumentNullException(nameof(baseDirectory));
            _createMissingDirectories = createMissingDirectories;

            if (!_baseDirectory.Exists)
            {
                throw new ArgumentException("Base outout directory not found.", nameof(baseDirectory),
                    new DirectoryNotFoundException(baseDirectory.FullName));
            }
        }

        public FileInfo SendMessage((TopicMessage Message, XElement Xml) message)
        {
            var rootEntity = message.Message.RootEntity;
            var entityType = rootEntity.EntityType;
            var outputDirectoryPath = Path.Combine(_baseDirectory.FullName, entityType.Id);

            if (!Directory.Exists(outputDirectoryPath))
            {
                CreateMissingOutputDirectory(outputDirectoryPath, entityType);
            }

            var outputPath = Path.Combine(outputDirectoryPath, $"{entityType.Id}_{rootEntity.Id}_{DateTime.Now.ToFileTimeUtc()}.xml");

            try
            {
                message.Xml.Save(outputPath);
            }
            catch (Exception e)
            {
                throw new Exception(
                    $"Failed to save file '{outputPath}'.",
                    e);
            }

            return new FileInfo(outputPath);
        }

        private void CreateMissingOutputDirectory(string outputPath, EntityType entityType)
        {
            if (!_createMissingDirectories)
            {
                throw new Exception($"Output directory missing for EntityType {entityType.Id}");
            }

            try
            {
                Directory.CreateDirectory(outputPath);
            }
            catch (Exception e)
            {
                throw new Exception(
                    $"Could not create missing output directory for EntityType {entityType.Id}.",
                    e);
            }
        }
    }
}

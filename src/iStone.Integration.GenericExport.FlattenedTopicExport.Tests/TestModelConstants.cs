﻿using System;
using System.Globalization;
using System.Linq;
using iStone.inRiver.Core;
using iStone.inRiver.Tests;
using iStone.inRiver.Tests.Builders;
using Moq;

namespace iStone.Integration.GenericExport.FlattenedTopicExport.Tests
{
    public static class TestModelConstants
    {
        public const string ChannelNode = "ChannelNode";
        public const string Product = "Product";
        public const string Item = "Item";
        public const string Resource = "Resource";

        public const string ProductId = "ProductId";
        public const string ResourceFileId = "ResourceFileId";
        public const string ResourceFilename = "ResourceFilename";

        public const string ChannelNodeProduct = "ChannelNodeProduct";
        public const string ProductItem = "ProductItem";
        public const string ProductResource = "ProductResource";
        public const string ItemResource = "ItemResource";

        public static PimApiMock CreateMockPimApi()
        {
            var modelBuilder = new DataModelBuilder()
                .WithEntityType(entityTypeBuilder => entityTypeBuilder
                    .SetId(ChannelNode)
                    .WithName(AddEnglishName(ChannelNode)))
                .WithEntityType(entityTypeBuilder => entityTypeBuilder
                    .SetId(Product)
                    .WithName(AddEnglishName(Product))
                    .WithFieldType(fieldTypeBuilder => fieldTypeBuilder
                        .SetId(ProductId)
                        .SetDataType(DataType.Integer)))
                .WithEntityType(entityTypeBuilder => entityTypeBuilder
                    .SetId(Item)
                    .WithName(AddEnglishName(Item)))
                .WithEntityType(entityTypeBuilder => entityTypeBuilder
                    .SetId(Resource)
                    .WithName(AddEnglishName(Resource))
                    .WithFieldType(fieldTypeBuilder => fieldTypeBuilder
                        .SetId(ResourceFileId)
                        .SetDataType(DataType.File))
                    .WithFieldType(fieldTypeBuilder => fieldTypeBuilder
                        .SetId(ResourceFilename)
                        .SetDataType(DataType.String)))
                .WithLinkType(linkTypeBuilder => linkTypeBuilder
                    .SetId(ChannelNodeProduct)
                    .SetSourceEntityType(ChannelNode)
                    .SetTargetEntityType(Product))
                .WithLinkType(linkTypeBuilder => linkTypeBuilder
                    .SetId(ProductItem)
                    .SetSourceEntityType(Product)
                    .SetTargetEntityType(Item))
                .WithLinkType(linkTypeBuilder => linkTypeBuilder
                    .SetId(ProductResource)
                    .SetSourceEntityType(Product)
                    .SetTargetEntityType(Resource))
                .WithLinkType(linkTypeBuilder => linkTypeBuilder
                    .SetId(ItemResource)
                    .SetSourceEntityType(Item)
                    .SetTargetEntityType(Resource));

            var mockPimApi = new PimApiMock(modelBuilder.Build());
            mockPimApi.ModelGetterMock
                .Setup(s => s.GetLinkTypesForEntityType(It.IsAny<string>()))
                .Returns<string>(id => mockPimApi.ModelGetter.GetAllLinkTypes()
                    .Where(linkType => linkType.SourceEntityTypeId == id ||
                                       linkType.TargetEntityTypeId == id)
                    .ToList());

            return mockPimApi;
        }

        public static readonly CultureInfo EnglishLanguage = new CultureInfo("en");

        private static Action<ILocaleStringBuilder> AddEnglishName(string name)
        {
            return builder => builder.ForLanguage(EnglishLanguage).SetText(name);
        }
    }
}
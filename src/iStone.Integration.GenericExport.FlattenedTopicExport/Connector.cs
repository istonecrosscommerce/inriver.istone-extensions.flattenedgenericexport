﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using inRiver.Integration.Export;
using inRiver.Integration.Interface;
using inRiver.Remoting.Image;
using iStone.inRiver.Core.Client;
using iStone.inRiver.Core.Client.Services.PIM.ServiceWrappers;
using iStone.inRiver.Core.Extensions;
using iStone.inRiver.Core.Services;
using iStone.inRiver.Portability.Objects;
using iStone.Integration.GenericExport.Criteria;
using iStone.Integration.GenericExport.EntityEvents;
using iStone.Integration.GenericExport.FlattenedTopicExport.Criteria;
using iStone.Integration.GenericExport.FlattenedTopicExport.Exceptions;
using iStone.Integration.GenericExport.FlattenedTopicExport.MessageFormatting;
using iStone.Integration.GenericExport.FlattenedTopicExport.MessageSending;
using iStone.Integration.GenericExport.FlattenedTopicExport.Utility;
using iStone.Integration.GenericExport.MessageSending;
using iStone.Integration.GenericExport.TopicMessages;
using iStone.Integration.GenericExport.Topics;
using Entity = inRiver.Remoting.Objects.Entity;

namespace iStone.Integration.GenericExport.FlattenedTopicExport
{
    public class Connector : ServerListener, IOutboundConnector, IEntityListener, ILinkListener
    {
        private const string TopicConfigurationFilePathKey = "TOPIC_CONFIGURATION_FILE";
        private const string TopicNameDescriptionLanguageKey = "TOPIC_NAME_DESCRIPTION_LANGUAGE";
        private const string NonRootEntityTypesKey = "NON_ROOT_ENTITY_TYPES";
        private const string OutputBaseDirectoryKey = "OUTPUT_BASE_DIRECTORY";
        private const string ImageOutputDirectoryKey = "IMAGE_OUTPUT_DIRECTORY";
        private const string ResourceConfigurationKey = "RESOURCE_CONFIGURATION";
        private const string CreateMissingDirectoriesKey = "CREATE_MISSING_DIRECTORIES";
        private const string ChannelIdKey = "CHANNEL_ID";

        private readonly PimApi _pimApi;

        private MultiMessageEntityEventExporter<IReadOnlyList<FileInfo>> _entityEventExporter;

        public Connector() : this(CreatePimApiWithIntegrationLogger()) { }
        public Connector(PimApi pimApi)
        {
            _pimApi = pimApi ?? throw new ArgumentException(nameof(pimApi));
        }

        public override void InitConfigurationSettings()
        {
            var settingsManager = _pimApi.GetConnectorSettingsManager(Id);
            settingsManager.ClearSetting(TopicConfigurationFilePathKey);
            settingsManager.SetSetting(TopicNameDescriptionLanguageKey, _pimApi.GetMasterLanguage());
            settingsManager.ClearSetting(NonRootEntityTypesKey);
            settingsManager.ClearSetting(OutputBaseDirectoryKey);
            settingsManager.SetSetting(ImageOutputDirectoryKey, "Resource/Images");
            settingsManager.SetSetting(CreateMissingDirectoriesKey, false);
            settingsManager.SetSetting(ResourceConfigurationKey, ImageConfiguration.Original);
            settingsManager.ClearSetting(ChannelIdKey);
            base.InitConfigurationSettings();
        }

        void IOutboundConnector.Start()
        {
            _pimApi.Logger.Information("Integration starting up.");

            try
            {
                SettingsGetter settingsGetter = _pimApi.GetConnectorSettingsManager(Id);
                var masterLanguage = _pimApi.GetMasterLanguage();

                var topicMessagesCreator = CreateTopicMessagesCreator(settingsGetter, masterLanguage);
                var messageSender = CreateMessageSender(settingsGetter, masterLanguage);
                var messageSentResultHandler = new LogFileSavedMessageSentResultHandler(_pimApi.Logger);

                _entityEventExporter = new MultiMessageEntityEventExporter<IReadOnlyList<FileInfo>>(_pimApi,
                    new BasicEntityEventCreator(Id), topicMessagesCreator,
                    messageSender, messageSentResultHandler);

                base.Start();
                _pimApi.Logger.Information("Integration has started.");
            }
            catch (Exception ex)
            {
                _pimApi.Logger.Error(ex, "Error during integration startup.");
                ((IOutboundConnector)this).Stop();
            }
        }

        void IOutboundConnector.Stop()
        {
            _pimApi.Logger.Information("Integration shutting down.");
            base.Stop();
            _pimApi.Logger.Information("Integration shut down.");
        }

        private FlattenedTopicMessagesCreator CreateTopicMessagesCreator(SettingsGetter settingsGetter, CultureInfo masterLanguage)
        {
            var nonRootEntityTypes = settingsGetter.GetSettingAsCollection(NonRootEntityTypesKey,
                ";", allowEmptyCollection: true, converter: _pimApi.ModelGetter.GetNonNullEntityType);

            var channelId = settingsGetter.GetSetting(ChannelIdKey, GetChannelId);

            var criteria = new EntityInConfiguredChannelCriteria(channelId, _pimApi);

            var topicNameDescriptionLanguage = settingsGetter.GetSetting(TopicNameDescriptionLanguageKey,
                languageName => new CultureInfo(languageName));

            var topicFlattener = new TopicFlattener(_pimApi, new CriteriaFactory(_pimApi.ModelGetter, masterLanguage),
                nonRootEntityTypes, criteria, topicNameDescriptionLanguage);

            var originalTopic = settingsGetter.GetSetting(TopicConfigurationFilePathKey,
                path => LoadTopic(path, masterLanguage));

            var blockedFieldTypes = new FieldType[0];

            var flattenedTopics = topicFlattener.FlattenTopic(originalTopic,
                new EntityType[0], blockedFieldTypes, new LinkType[0]);

            var topicMessagesCreator = new FlattenedTopicMessagesCreator(_pimApi, flattenedTopics,
                new TopicMessageCreator(blockedFieldTypes));
            return topicMessagesCreator;
        }

        private int GetChannelId(string channelIdString)
        {
            if (!int.TryParse(channelIdString, out var entityId))
            {
                throw new Exception("Invalid entity id.");
            }

            var entity = _pimApi.DataGetter.GetNonNullEntity(entityId, LoadLevel.Shallow);
            if (entity.EntityType.Id != ModelConstants.Channel.EntityTypeId)
            {
                throw new Exception($"Expected Entity of type {ModelConstants.Channel.Id}, got {entity.EntityType.Id}");
            }

            return entity.Id;
        }

        private Topic LoadTopic(string topicConfigurationFilePath, CultureInfo masterLanguage)
        {
            var topicsDocument = XDocument.Load(topicConfigurationFilePath);
            var topicCreator = new TopicCreator(
                Enumerable.Empty<EntityType>(), Enumerable.Empty<FieldType>(), Enumerable.Empty<LinkType>(),
                masterLanguage, _pimApi.UtilityGetter.GetAllLanguages(), _pimApi);

            var topics = topicCreator.CreateTopics(topicsDocument);
            if (topics.Count != 1)
            {
                throw new Exception($"Expected single configured topic, got {topics.Count}.");
            }

            return topics.Single();
        }

        private IMessageSender<List<FileInfo>> CreateMessageSender(SettingsGetter settingsGetter,
            CultureInfo masterLanguage)
        {
            var messageTransformer = XmlAndTopicMessagesMessageTransformer.CreateDefault(
                masterLanguage, _pimApi.UtilityGetter.GetAllLanguages());

            var baseDirectory = settingsGetter.GetSetting(OutputBaseDirectoryKey,
                path => new DirectoryInfo(GetExistingDirectoryPath(path)));

            var createMissingDirectories = settingsGetter.GetSetting<bool>(CreateMissingDirectoriesKey);

            var messageToFolderWriter = new PerEntityTypeWriteXmlToFolderMessageSender(
                baseDirectory, createMissingDirectories);

            var imageOutputPath = settingsGetter.GetSetting<string>(ImageOutputDirectoryKey);

            var imageOutputDirectory = Path.IsPathRooted(imageOutputPath)
                ? new DirectoryInfo(imageOutputPath)
                : new DirectoryInfo(Path.Combine(baseDirectory.FullName, imageOutputPath));

            var resourceConfiguration = settingsGetter.GetSetting(ResourceConfigurationKey, GetNonNullResourceConfiguration);

            var imageToFolderWriter = new WriteImageToFolderMessageSender(_pimApi,
                resourceConfiguration, imageOutputDirectory, createMissingDirectories);

            var simpleMessageSender = new WriteFilesMessageSender(
                new ISimpleMessageSender<(TopicMessage Message, XElement Xml), FileInfo>[]
                    { messageToFolderWriter, imageToFolderWriter });

            var messageSender = new MessageSenderWithTransformer<ValueTuple<TopicMessage, XElement>, List<FileInfo>>(
                messageTransformer, simpleMessageSender);
            return messageSender;
        }

        private static string GetExistingDirectoryPath(string path)
        {
            return Directory.Exists(path)
                ? path
                : throw new DirectoryNotFoundException(path);
        }

        private string GetNonNullResourceConfiguration(string configuration)
        {
            return _pimApi.UtilityService.GetAllImageConfigurations().Contains(configuration)
                ? configuration
                : throw new ImageConfigurationNotFoundException(configuration);
        }

        private static PimApi CreatePimApiWithIntegrationLogger()
        {
            var api = new PimApi();
            api.Logger = new IntegrationLogger(new PimIntegrationLogger(api));
            return api;
        }

        #region IEntityListener

        public void EntityCreated(int entityId)
        {
            try
            {
                _entityEventExporter.EntityCreated(entityId);
            }
            catch (Exception e)
            {
                _pimApi.Logger.Error($"Failed to handle EntityCreated event for entity {entityId}.", e);
            }
        }

        public void EntityUpdated(int entityId, string[] fields)
        {
            try
            {
                _entityEventExporter.EntityUpdated(entityId, fields);
            }
            catch (Exception e)
            {
                _pimApi.Logger.Error(
                    $"Failed to handle EntityUpdated event for entity {entityId} and fields ({string.Join(", ", fields)}).",
                    e);
            }
        }

        public void EntityDeleted(Entity deletedEntity)
        {
            try
            {
                _entityEventExporter.EntityDeleted(deletedEntity);
            }
            catch (Exception e)
            {
                _pimApi.Logger.Error(
                    $"Failed to handle EntityDeleted event for {deletedEntity.EntityType.Id} {deletedEntity.Id}.",
                    e);
            }
        }

        #region Unused interface members

        public void EntityCommentAdded(int entityId, int commentId)
        {
        }
        public void EntityFieldSetUpdated(int entityId, string fieldSetId)
        {
        }
        public void EntityLocked(int entityId)
        {
        }
        public void EntitySpecificationFieldAdded(int entityId, string fieldName)
        {
        }
        public void EntitySpecificationFieldUpdated(int entityId, string fieldName)
        {
        }
        public void EntityUnlocked(int entityId)
        {
        }

        #endregion
        #endregion
        #region ILinkListener

        public void LinkCreated(int linkId, int sourceId, int targetId, string linkTypeId, int? linkEntityId)
        {
            try
            {
                _entityEventExporter.LinkCreated(linkId, sourceId, targetId, linkTypeId, linkEntityId);
            }
            catch (Exception e)
            {
                _pimApi.Logger.Error(
                    $"Failed to handle LinkCreated event for link {linkId}, source {sourceId} and target {targetId}.",
                    e);
            }
        }

        public void LinkUpdated(int linkId, int sourceId, int targetId, string linkTypeId, int? linkEntityId)
        {
            try
            {
                _entityEventExporter.LinkUpdated(linkId, sourceId, targetId, linkTypeId, linkEntityId);
            }
            catch (Exception e)
            {
                _pimApi.Logger.Error(
                    $"Failed to handle LinkUpdated event for link {linkId}, source {sourceId} and target {targetId}.",
                    e);
            }
        }

        public void LinkDeleted(int linkId, int sourceId, int targetId, string linkTypeId, int? linkEntityId)
        {
            try
            {
                _entityEventExporter.LinkDeleted(linkId, sourceId, targetId, linkTypeId, linkEntityId);
            }
            catch (Exception e)
            {
                _pimApi.Logger.Error(
                    $"Failed to handle LinkDeleted event for link {linkId}, source {sourceId} and target {targetId}.",
                    e);
            }
        }

        #region Unused interface members

        public void LinkActivated(int linkId, int sourceId, int targetId, string linkTypeId, int? linkEntityId)
        {
        }
        public void LinkInactivated(int linkId, int sourceId, int targetId, string linkTypeId, int? linkEntityId)
        {
        }

        #endregion
        #endregion
    }
}

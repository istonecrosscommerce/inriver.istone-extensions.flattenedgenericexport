﻿using System;
using System.Collections.Generic;
using System.Linq;
using iStone.inRiver.Core;
using iStone.inRiver.Core.Comparers;
using iStone.inRiver.Core.Extensions;
using iStone.inRiver.Portability.Objects;
using iStone.Integration.GenericExport.Criteria;
using iStone.Integration.GenericExport.Topics;
using NUnit.Framework;

namespace iStone.Integration.GenericExport.FlattenedTopicExport.Tests
{
    [TestFixture]
    public class TopicFlattenerTest
    {
        private IPimApi _pimApi;
        private CriteriaFactory _criteriaFactory;
        private ICriteria _rootCriteria;
        private TopicFlattener _topicFlattener;

        [SetUp]
        public void Setup()
        {
            _pimApi = TestModelConstants.CreateMockPimApi().PimApi;
            _criteriaFactory = new CriteriaFactory(_pimApi.ModelGetter, TestModelConstants.EnglishLanguage);
            _rootCriteria = _criteriaFactory.CreateAlwaysTrueCriteria();
            _topicFlattener = new TopicFlattener(_pimApi, _criteriaFactory, Enumerable.Empty<EntityType>(), _rootCriteria,
                TestModelConstants.EnglishLanguage);
        }

        [Test]
        public void ProductTopic()
        {
            var topicFlattener = new TopicFlattener(_pimApi, _criteriaFactory, Enumerable.Empty<EntityType>(),
                _rootCriteria, TestModelConstants.EnglishLanguage);
            
            var flattenedTopics = CreateFlattenedTopics(SimpleEntityGraphNode.ProductNode);

            AssertThatFlattenedTopicHasValidStructure(flattenedTopics);
            AssertThatStructureIsEqual(new[] { SimpleEntityGraphNode.ProductNode }, flattenedTopics.FlattendedTopicsConfiguration, flattenedTopics.GetNonRootEntityTypeIds());
            AssertThatStructureIsEqual(new[] { SimpleEntityGraphNode.ProductNode }, flattenedTopics.OriginalSubTopicsConfiguration, flattenedTopics.GetNonRootEntityTypeIds());
        }

        [Test]
        public void ProductItemTopic()
        {
            var topicFlattener = new TopicFlattener(_pimApi, _criteriaFactory, Enumerable.Empty<EntityType>(),
                _rootCriteria, TestModelConstants.EnglishLanguage);

            var flattenedTopics = CreateFlattenedTopics(SimpleEntityGraphNode.ProductItemNode);

            AssertThatFlattenedTopicHasValidStructure(flattenedTopics);
            AssertThatStructureIsEqual(new[] { SimpleEntityGraphNode.ProductItemNode, SimpleEntityGraphNode.ItemNode }, flattenedTopics.FlattendedTopicsConfiguration, flattenedTopics.GetNonRootEntityTypeIds());
            AssertThatStructureIsEqual(new[] { SimpleEntityGraphNode.ProductItemNode, SimpleEntityGraphNode.ItemNode }, flattenedTopics.OriginalSubTopicsConfiguration, flattenedTopics.GetNonRootEntityTypeIds());
        }

        [Test]
        public void ProductItemResourceTopic()
        {
            var topicFlattener = new TopicFlattener(_pimApi, _criteriaFactory, Enumerable.Empty<EntityType>(),
                _rootCriteria, TestModelConstants.EnglishLanguage);

            var flattenedTopics = CreateFlattenedTopics(SimpleEntityGraphNode.ProductItemResourceNode);

            AssertThatFlattenedTopicHasValidStructure(flattenedTopics);
            AssertThatStructureIsEqual(new[] { SimpleEntityGraphNode.FlatProductItemResourceNode, SimpleEntityGraphNode.ItemResourceNode, SimpleEntityGraphNode.ResourceNode }, flattenedTopics.FlattendedTopicsConfiguration, flattenedTopics.GetNonRootEntityTypeIds());
            AssertThatStructureIsEqual(new[] { SimpleEntityGraphNode.ProductItemResourceNode, SimpleEntityGraphNode.ItemResourceNode, SimpleEntityGraphNode.ResourceNode }, flattenedTopics.OriginalSubTopicsConfiguration, flattenedTopics.GetNonRootEntityTypeIds());
        }

        [Test]
        public void ProductItemResourceTopicWithNonRootChannelNode()
        {
            var flattenedTopics = CreateFlattenedTopics(SimpleEntityGraphNode.ProductItemResourceChannelNodeNode,
                new[] {TestModelConstants.ChannelNode} );

            AssertThatFlattenedTopicHasValidStructure(flattenedTopics);
            AssertThatStructureIsEqual(new[] { SimpleEntityGraphNode.FlatProductItemResourceChannelNodeNode, SimpleEntityGraphNode.ItemResourceNode, SimpleEntityGraphNode.ResourceNode }, flattenedTopics.FlattendedTopicsConfiguration, flattenedTopics.GetNonRootEntityTypeIds());
            AssertThatStructureIsEqual(new[] { SimpleEntityGraphNode.ProductItemResourceChannelNodeNode, SimpleEntityGraphNode.ItemResourceNode, SimpleEntityGraphNode.ResourceNode }, flattenedTopics.OriginalSubTopicsConfiguration, flattenedTopics.GetNonRootEntityTypeIds());
        }

        [Test]
        public void MergeGraphNodes()
        {
            var flattenedTopics = CreateFlattenedTopics(SimpleEntityGraphNode.ProductResourceWithFieldTypesNode);

            AssertThatFlattenedTopicHasValidStructure(flattenedTopics);
            AssertThatStructureIsEqual(new[] { SimpleEntityGraphNode.ProductResourceNode, SimpleEntityGraphNode.ResourceWithFileIdAndNameNode }, flattenedTopics.FlattendedTopicsConfiguration, flattenedTopics.GetNonRootEntityTypeIds());
            AssertThatStructureIsEqual(new[] { SimpleEntityGraphNode.ProductResourceWithFieldTypesMergedNode, SimpleEntityGraphNode.ResourceWithFileIdAndNameNode }, flattenedTopics.OriginalSubTopicsConfiguration, flattenedTopics.GetNonRootEntityTypeIds());
        }

        #region Helpers

        private FlattenedTopics CreateFlattenedTopics(SimpleEntityGraphNode root, IEnumerable<string> nonRootEntityTypeIds = null)
        {
            var topic = SimpleEntityGraphNode.CreateTopic(_pimApi, root);

            var topicFlattener = _topicFlattener;
            if (nonRootEntityTypeIds != null)
            {
                var nonRootEntityTypes = nonRootEntityTypeIds
                    .Select(_pimApi.ModelService.GetNonNullEntityType)
                    .ToList();
                topicFlattener = new TopicFlattener(_pimApi, _criteriaFactory, nonRootEntityTypes, _rootCriteria,
                    TestModelConstants.EnglishLanguage);
            }

            return topicFlattener.FlattenTopic(topic,
                new EntityType[0], new FieldType[0], new LinkType[0]);
        }

        private void AssertThatFlattenedTopicHasValidStructure(FlattenedTopics flattenedTopics)
        {
            var flatTopicsPerEntityType = flattenedTopics.FlattendedTopicsConfiguration.Topics
                .ToLookup(topic => topic.Root.EntityType.Id)
                .ToDictionary(topics => topics.Key, topics => topics.ToList());

            foreach (var kvp in flatTopicsPerEntityType)
            {
                var entityTypeId = kvp.Key;
                var topics = kvp.Value;

                Assert.That(topics.Count, Is.EqualTo(1), () => $"Multiple topics for EntityType {entityTypeId}.");

                var topic = topics.Single();
                Assert.That(flattenedTopics.NonRootEntityTypes, Does.Not.Contain(topic.Root.EntityType)
                                                .Using((IEqualityComparer<EntityType>)new EntityTypeIdComparer()),
                    $"Flat root found for non root EntityType {entityTypeId}");

                var grandChildren = topic.Root.Children
                    .SelectMany(child => child.GraphNode.Children)
                    .ToList();
                Assert.That(grandChildren, Is.Empty, $"Topic for EntityType {entityTypeId} has grandchildren EntityTypeGraphNodes");
            }

            var originalSubTopicsPerEntityType = flattenedTopics.OriginalSubTopicsConfiguration.Topics
                .Where(topic => !flattenedTopics.NonRootEntityTypes.Contains(topic.Root.EntityType, new EntityTypeIdComparer()))
                .ToLookup(topic => topic.Root.EntityType.Id)
                .ToDictionary(topics => topics.Key, topics => topics.ToList());

            Assert.That(flatTopicsPerEntityType.Keys, Is.EquivalentTo(originalSubTopicsPerEntityType.Keys));
        }

        private static void AssertThatStructureIsEqual(IEnumerable<SimpleEntityGraphNode> expected, TopicConfiguration actual,
            IEnumerable<string> nonRootEntityTypeIds)
        {
            var expectedSimpleRootPerEntityType = expected
                .Where(node => !nonRootEntityTypeIds.Contains(node.EntityTypeId))
                .ToDictionary(node => node.EntityTypeId);

            var actualTopicPerEntityType = actual.Topics
                .ToDictionary(topic => topic.Root.EntityType.Id, topic => topic.Root);

            Assert.That(actualTopicPerEntityType.Keys, Is.EqualTo(expectedSimpleRootPerEntityType.Keys));

            foreach (var entityTypeId in actualTopicPerEntityType.Keys)
            {
                AssertThatStructureIsEqual(expectedSimpleRootPerEntityType[entityTypeId], actualTopicPerEntityType[entityTypeId]);
            }
        }

        private static void AssertThatStructureIsEqual(SimpleEntityGraphNode expected, EntityTypeGraphNode actual)
        {
            Assert.That(actual.EntityType.Id, Is.EqualTo(expected.EntityTypeId));
            Assert.That(actual.Children.Count, Is.EqualTo(expected.Children.Count));
            Assert.That(actual.FieldTypes.Select(fieldType => fieldType.Id), Is.EquivalentTo(expected.FieldTypeIds));

            foreach (var childPair in expected.Children.Zip(actual.Children, Tuple.Create))
            {
                AssertThatStructureIsEqual(childPair.Item1, childPair.Item2.GraphNode);
            }
        }

        #endregion
    }
}

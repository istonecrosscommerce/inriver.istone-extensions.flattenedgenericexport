﻿using System.Collections.Generic;
using System.Linq;
using iStone.inRiver.Core;
using iStone.inRiver.Core.Extensions;
using iStone.inRiver.Portability.Objects;
using iStone.inRiver.Tests.Extensions;
using iStone.inRiver.Tests.Interface;
using iStone.inRiver.Tests.Mocks;
using iStone.Integration.GenericExport.Criteria;
using iStone.Integration.GenericExport.EntityEvents;
using iStone.Integration.GenericExport.TopicMessages;
using iStone.Integration.GenericExport.Utilities;
using NUnit.Framework;

namespace iStone.Integration.GenericExport.FlattenedTopicExport.Tests
{
    public class FlattenedTopicMessagesCreatorTest
    {
        private static readonly IEntityEventCreator EntityEventCreator = new BasicEntityEventCreator("TestConnector");
        private static readonly ITopicMessageCreator TopicMessageCreator = new TopicMessageCreator(
            Enumerable.Empty<FieldType>());

        private IPimApi _pimApi;
        private CriteriaFactory _criteriaFactory;
        private ICriteria _rootCriteria;
        private TopicFlattener _topicFlattener;

        [SetUp]
        public void Setup()
        {
            var mockPimApi = TestModelConstants.CreateMockPimApi();

            _pimApi = mockPimApi.PimApi;
            _criteriaFactory = new CriteriaFactory(_pimApi.ModelGetter, TestModelConstants.EnglishLanguage);
            _rootCriteria = _criteriaFactory.CreateAlwaysTrueCriteria();
            _topicFlattener = new TopicFlattener(_pimApi, _criteriaFactory, Enumerable.Empty<EntityType>(), _rootCriteria,
                TestModelConstants.EnglishLanguage);

            _pimApi.Logger = new LoggerMock().GetMock().Object;

            AddEntitiesAndLinks(mockPimApi);

        }

        // Disabled due to nullref in GenericExport:
        //[TestCase(NonEntityIdIndex, TestModelConstants.ProductId, new string[0], TestName="Field update for non existant entity in topic results in no export")]
        [TestCase(ProductEntityIdIndex, TestModelConstants.ProductId, new[] { TestModelConstants.Product }, TestName = "Field update for Product in topic results in Product exported")]
        [TestCase(ProductResourceEntityIdIndex, TestModelConstants.ResourceFileId, new[] { TestModelConstants.Resource }, TestName = "Field update for Resource in topic results in Resource exported")]
        [TestCase(ItemResourceEntityIdIndex, TestModelConstants.ResourceFileId, new string[0], TestName = "Field update for Resource in topic but not linked to Product results in no export")]
        public void ProductResourceTopicEntityUpdated(int entityIdIndex, string fieldTypeId, string[] expectedRootEntityTypeIds)
        {
            var entityId = _entityIds[entityIdIndex];
            var topicMessages = CreateFlattenedTopicMessagesForEntityUpdated(SimpleEntityGraphNode.ProductResourceWithFieldTypesNode,
                new string[0], entityId, fieldTypeId);

            var actualRootEntityTypeIds = topicMessages
                .Select(topicMessage => topicMessage.RootEntity.EntityType.Id)
                .ToList();

            Assert.That(actualRootEntityTypeIds, Is.EquivalentTo(expectedRootEntityTypeIds));
        }

        // Disabled due to nullref in GenericExport:
        //[TestCase(NonLinkIdIndex, new string[0], TestName = "Link created for non existant link results in no export")]
        [TestCase(ProductResourceLinkIdIndex, new[] { TestModelConstants.Product, TestModelConstants.Resource }, TestName = "Link created in topic results in source and target exported")]
        [TestCase(ProductItemLinkIdIndex, new string[0], TestName = "Link created with only source in topic results in no export")]
        [TestCase(ItemResourceLinkIdIndex, new string[0], TestName = "Link created with only target in topic results in no export")]
        [TestCase(UnlinkedItemResourceLinkIdIndex, new string[0], TestName = "Link created with neither source or target in topic results in no export")]
        public void ProductResourceTopicLinkCreated(int linkIdIndex, string[] expectedRootEntityTypeIds)
        {
            var link = _links[linkIdIndex];
            var topicMessages = CreateFlattenedTopicMessagesForLinkCreated(SimpleEntityGraphNode.ProductResourceNode,
                new string[0], link.Id, link.Source.Id, link.Target.Id, link.LinkType.Id);

            var actualRootEntityTypeIds = topicMessages
                .Select(topicMessage => topicMessage.RootEntity.EntityType.Id)
                .ToList();

            Assert.That(actualRootEntityTypeIds, Is.EquivalentTo(expectedRootEntityTypeIds));
        }
        
        [TestCase(ProductItemLinkIdIndex, new[] { TestModelConstants.Product, TestModelConstants.Item, TestModelConstants.Resource }, TestName = "Link created in topic with target with child entity link results in source, target and child entity exported")]
        [TestCase(ItemResourceLinkIdIndex, new[] { TestModelConstants.Item, TestModelConstants.Resource }, TestName = "Link created in topic with source as child of root results in source and target exported")]
        [TestCase(UnlinkedItemResourceLinkIdIndex, new string[0], TestName = "Link created in topic with source not linked to root results in no export")]
        public void ProductItemResourceTopicLinkCreated(int linkIdIndex, string[] expectedRootEntityTypeIds)
        {
            var link = _links[linkIdIndex];
            var topicMessages = CreateFlattenedTopicMessagesForLinkCreated(SimpleEntityGraphNode.ProductItemResourceNode,
                new string[0], link.Id, link.Source.Id, link.Target.Id, link.LinkType.Id);

            var actualRootEntityTypeIds = topicMessages
                .Select(topicMessage => topicMessage.RootEntity.EntityType.Id)
                .ToList();

            Assert.That(actualRootEntityTypeIds, Is.EquivalentTo(expectedRootEntityTypeIds));
        }

        // Disabled due to nullref in GenericExport:
        //[TestCase(NonLinkIdIndex, TestModelConstants.ProductId, new string[0])]
        [TestCase(ProductResourceLinkIdIndex, new[] { TestModelConstants.Product }, TestName = "Link deleted in topic with source as root results in source exported")]
        [TestCase(ItemResourceLinkIdIndex, new[] { TestModelConstants.Item, }, TestName = "Link deleted in topic with source as child of root results in source exported")]
        [TestCase(UnlinkedItemResourceLinkIdIndex, new string[0], TestName = "Link deleted in topic with source not linked to root results in no export")]
        public void ProductItemResourceTopicLinkDeleted(int linkIdIndex, string[] expectedRootEntityTypeIds)
        {
            var link = _links[linkIdIndex];

            // DeleteLink is not mocked in iStone.inRiver.Tests, and is difficult to mock from the outside.
            // However, if it is called or not does not affect the which roots are exported,
            // which is all that is currently tested
            //_pimApi.DataService.DeleteLink(link.Id);

            var topicMessages = CreateFlattenedTopicMessagesForLinkDeleted(SimpleEntityGraphNode.ProductItemResourceNode,
                new string[0], link.Id, link.Source.Id, link.Target.Id, link.LinkType.Id);

            var actualRootEntityTypeIds = topicMessages
                .Select(topicMessage => topicMessage.RootEntity.EntityType.Id)
                .ToList();

            Assert.That(actualRootEntityTypeIds, Is.EquivalentTo(expectedRootEntityTypeIds));
        }

        #region Helpers

        private const int NonEntityIdIndex = 0;
        private const int ProductEntityIdIndex = 1;
        private const int ProductResourceEntityIdIndex = 2;
        private const int ProductItemEntityIdIndex = 3;
        private const int ItemResourceEntityIdIndex = 4;
        private const int UnlinkedProductEntityIdIndex = 5;
        private const int UnlinkedItemEntityIdIndex = 6;
        private const int UnlinkedResourceEntityIdIndex = 7;

        private readonly Dictionary<int, int> _entityIds = new Dictionary<int, int>();

        private const int NonLinkIdIndex = 0;
        private const int ProductResourceLinkIdIndex = 1;
        private const int ProductItemLinkIdIndex = 2;
        private const int ItemResourceLinkIdIndex = 3;
        private const int UnlinkedItemResourceLinkIdIndex = 4;

        private readonly Dictionary<int, Link> _links = new Dictionary<int, Link>();

        private void AddEntitiesAndLinks(IPimMock mockPimApi)
        {
            var product = mockPimApi.AddEntity(TestModelConstants.Product,
                entityFieldBuilder => entityFieldBuilder.SetInteger(TestModelConstants.ProductId, 1));

            var productResource = mockPimApi.AddEntity(TestModelConstants.Resource,
                entityFieldBuilder => entityFieldBuilder.SetFile(TestModelConstants.ResourceFileId, 1));
            var productResourceLink = mockPimApi.AddLink(TestModelConstants.ProductResource, product, productResource);

            var productItem = mockPimApi.AddEntity(TestModelConstants.Item, _ => { });
            var productItemLink = mockPimApi.AddLink(TestModelConstants.ProductItem, product, productItem);

            var itemResource = mockPimApi.AddEntity(TestModelConstants.Resource,
                entityFieldBuilder => entityFieldBuilder.SetFile(TestModelConstants.ResourceFileId, 1));
            var itemResourceLink = mockPimApi.AddLink(TestModelConstants.ItemResource, productItem, itemResource);

            var unlinkedProduct = mockPimApi.AddEntity(TestModelConstants.Product,
                entityFieldBuilder => entityFieldBuilder.SetInteger(TestModelConstants.ProductId, 1));
            var unlinkedItem = mockPimApi.AddEntity(TestModelConstants.Item, _ => { });
            var unlinkedResource = mockPimApi.AddEntity(TestModelConstants.Resource,
                entityFieldBuilder => entityFieldBuilder.SetFile(TestModelConstants.ResourceFileId, 1));
            var unlinkedItemResourceLink = mockPimApi.AddLink(TestModelConstants.ItemResource, unlinkedItem, unlinkedResource);

            _entityIds[NonEntityIdIndex] = 123456;
            _entityIds[ProductEntityIdIndex] = product.Id;
            _entityIds[ProductResourceEntityIdIndex] = productResource.Id;
            _entityIds[ProductItemEntityIdIndex] = productItem.Id;
            _entityIds[ItemResourceEntityIdIndex] = itemResource.Id;
            _entityIds[UnlinkedProductEntityIdIndex] = unlinkedProduct.Id;
            _entityIds[UnlinkedItemEntityIdIndex] = unlinkedItem.Id;
            _entityIds[UnlinkedResourceEntityIdIndex] = unlinkedResource.Id;

            _links[NonLinkIdIndex] = null;
            _links[ProductResourceLinkIdIndex] = productResourceLink;
            _links[ProductItemLinkIdIndex] = productItemLink;
            _links[ItemResourceLinkIdIndex] = itemResourceLink;
            _links[UnlinkedItemResourceLinkIdIndex] = unlinkedItemResourceLink;
        }

        private List<TopicMessage> CreateFlattenedTopicMessages(SimpleEntityGraphNode root, IEnumerable<string> nonRootEntityTypeIds,
            EntityEvent entityEvent, EvaluationInfo evaluationInfo)
        {
            var topic = SimpleEntityGraphNode.CreateTopic(_pimApi, root);

            var topicFlattener = _topicFlattener;
            if (nonRootEntityTypeIds != null)
            {
                var nonRootEntityTypes = nonRootEntityTypeIds
                    .Select(_pimApi.ModelService.GetNonNullEntityType)
                    .ToList();
                topicFlattener = new TopicFlattener(_pimApi, _criteriaFactory, nonRootEntityTypes, _rootCriteria,
                    TestModelConstants.EnglishLanguage);
            }

            var flattenedTopics = topicFlattener.FlattenTopic(topic,
                new EntityType[0], new FieldType[0], new LinkType[0]);

            var topicMessagesCreator = new FlattenedTopicMessagesCreator(_pimApi, flattenedTopics, TopicMessageCreator);

            return topicMessagesCreator.CreateTopicMessages(entityEvent, evaluationInfo, new PimCache(_pimApi));
        }

        private List<TopicMessage> CreateFlattenedTopicMessagesForEntityUpdated(SimpleEntityGraphNode root,
            IEnumerable<string> nonRootEntityTypeIds, int entityId, string fieldTypeId)
        {
            var entityEvent = EntityEventCreator.EntityUpdated(entityId, new[] { fieldTypeId });
            var evaluationInfo = new EvaluationInfo(new[] { _pimApi.ModelGetter.GetNonNullFieldType(fieldTypeId) }, entityEvent);
            return CreateFlattenedTopicMessages(root, nonRootEntityTypeIds, entityEvent, evaluationInfo);
        }

        private List<TopicMessage> CreateFlattenedTopicMessagesForLinkCreated(SimpleEntityGraphNode root,
            IEnumerable<string> nonRootEntityTypeIds, int linkId, int sourceId, int targetId, string linkTypeId)
        {
            var entityEvent = EntityEventCreator.LinkCreated(linkId, sourceId, targetId, linkTypeId, null);
            var evaluationInfo = new EvaluationInfo(entityEvent);
            return CreateFlattenedTopicMessages(root, nonRootEntityTypeIds, entityEvent, evaluationInfo);
        }

        private List<TopicMessage> CreateFlattenedTopicMessagesForLinkDeleted(SimpleEntityGraphNode root,
            IEnumerable<string> nonRootEntityTypeIds, int linkId, int sourceId, int targetId, string linkTypeId)
        {
            var entityEvent = EntityEventCreator.LinkDeleted(linkId, sourceId, targetId, linkTypeId, null);
            var evaluationInfo = new EvaluationInfo(entityEvent);
            return CreateFlattenedTopicMessages(root, nonRootEntityTypeIds, entityEvent, evaluationInfo);
        }

        #endregion
    }
}

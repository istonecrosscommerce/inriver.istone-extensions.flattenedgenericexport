﻿using System;
using System.Collections.Generic;
using System.Linq;
using iStone.inRiver.Core;
using iStone.inRiver.Core.Comparers;
using iStone.inRiver.Core.Extensions;
using iStone.inRiver.Portability.Interface;
using iStone.inRiver.Portability.Objects;
using iStone.Integration.GenericExport.Criteria;
using iStone.Integration.GenericExport.EntityEvents;
using iStone.Integration.GenericExport.FlattenedTopicExport.Extensions;
using iStone.Integration.GenericExport.TopicMessages;
using iStone.Integration.GenericExport.Topics;
using iStone.Integration.GenericExport.Utilities;

namespace iStone.Integration.GenericExport.FlattenedTopicExport
{
    public class FlattenedTopicMessagesCreator : ITopicMessagesCreator
    {
        private readonly IPimApi _pimApi;
        private readonly IModelGetter _modelGetter;

        private readonly TopicRootEntityIdGetter _topicRootEntityIdGetter;
        private readonly IReadOnlyDictionary<EntityType, Topic> _linkCreatedTopics;
        private readonly ISet<EntityType> _nonRootEntityTypeIds;

        private readonly ITopicMessageCreator _topicMessageCreator;

        public FlattenedTopicMessagesCreator(IPimApi pimApi, FlattenedTopics flattenedTopics, ITopicMessageCreator topicMessageCreator)
        {
            _pimApi = pimApi ?? throw new ArgumentNullException(nameof(pimApi));
            _modelGetter = _pimApi.ModelGetter;

            if (flattenedTopics == null) throw new ArgumentNullException(nameof(flattenedTopics));

            _topicRootEntityIdGetter = new TopicRootEntityIdGetter(pimApi,
                flattenedTopics.FlattendedTopicsConfiguration);

            _linkCreatedTopics = flattenedTopics.OriginalSubTopicsConfiguration.Topics
                .ToDictionary(topic => topic.Root.EntityType, new EntityTypeIdComparer());

            _nonRootEntityTypeIds =
                new HashSet<EntityType>(flattenedTopics.NonRootEntityTypes, new EntityTypeIdComparer());

            _topicMessageCreator = topicMessageCreator ?? throw new ArgumentNullException(nameof(topicMessageCreator));
        }

        public List<TopicMessage> CreateTopicMessages(EntityEvent entityEvent, EvaluationInfo evaluationInfo, PimCache pimCache)
        {
            _pimApi.Logger.Verbose($"Creating flattened topic messages for EntityEvent: {entityEvent}");

            var rootEntityIds = _topicRootEntityIdGetter.GetRootEntityIds(entityEvent, evaluationInfo, pimCache)
                .CreateNonDeferred();

            _pimApi.Logger.Verbose($"Original root entity ids: {string.Join(", ", rootEntityIds.AllRootEntityIds)}");

            var topicMessages = CreateTopicMessages(entityEvent, rootEntityIds, pimCache);

            var allRootEntityIds = topicMessages
                .Select(topicMessage => topicMessage.RootEntity.Id)
                .ToList();

            _pimApi.Logger.Verbose($"Created {allRootEntityIds.Count} flattened topic messages, for entities: ({string.Join(", ", allRootEntityIds)})");

            return topicMessages;
        }

        private List<TopicMessage> CreateTopicMessages(EntityEvent entityEvent, TopicRootEntityIdCollection rootEntityIds,
            PimCache pimCache)
        {
            if (!entityEvent.IsLinkCreatedEvent())
            {
                return _topicMessageCreator.CreateTopicMessages(rootEntityIds,
                    new Dictionary<int, List<Guid>>(), new[] { entityEvent });
            }

            var rootEntityId = GetRootEntityId(rootEntityIds);
            if (!rootEntityId.HasValue)
            {
                return new List<TopicMessage>();
            }

            var triggeredByOutboundLink = entityEvent.LinkEventInfo.SourceEntityId == rootEntityId;

            var sourceTopicMessage = CreateTopicMessage(entityEvent, pimCache, triggeredByOutboundLink);
            var targetTopicMessage = CreateTopicMessage(entityEvent, pimCache, !triggeredByOutboundLink);

            if (targetTopicMessage == null) // target would use a non root topic
            {
                return FlattenTopicMessages(sourceTopicMessage).ToList();
            }

            return FlattenTopicMessages(targetTopicMessage)
                .Prepend(sourceTopicMessage)
                .DistinctBy(topicMessage => topicMessage.RootEntity.Id)
                .ToList();
        }

        private static int? GetRootEntityId(TopicRootEntityIdCollection rootEntityIds)
        {
            return rootEntityIds.RootEntityIds
                .Where(kvp => kvp.Value.Any())
                .Select(kvp => (int?)kvp.Value.Single())
                .SingleOrDefault();

        }

        private TopicMessage CreateTopicMessage(EntityEvent entityEvent, PimCache pimCache, bool useSource)
        {
            var linkEventInfo = entityEvent.LinkEventInfo;
            var linkType = _modelGetter.GetNonNullLinkType(linkEventInfo.LinkTypeId);

            var entityTypeId = useSource
                ? linkType.SourceEntityTypeId
                : linkType.TargetEntityTypeId;
            var entityType = _modelGetter.GetNonNullEntityType(entityTypeId);
            var topic = GetTopic(entityType);
            if (topic == null)
            {
                return null;
            }

            var rootEntityId = useSource
                ? linkEventInfo.SourceEntityId
                : linkEventInfo.TargetEntityId;

            return _topicMessageCreator.CreateTopicMessage(topic, rootEntityId.Value,
                Enumerable.Empty<Guid>(), pimCache, new EvaluationCache(), new[] { entityEvent });
        }

        private Topic GetTopic(EntityType entityType)
        {
            return _linkCreatedTopics.TryGetValue(entityType, out var topic)
                ? topic
                : null;
        }

        private IEnumerable<TopicMessage> FlattenTopicMessages(TopicMessage topicMessage)
        {
            return GetAllEntityViews(topicMessage.RootEntity)
                .Where(entityView => !_nonRootEntityTypeIds.Contains(entityView.EntityType))
                .Select(entityView => CopyEntityView(entityView, root: true))
                .Select(entityView => TopicMessage.Create(Guid.NewGuid(), topicMessage.TimeStamp,
                    topicMessage.Topic, entityView, topicMessage.RegisterTokens, topicMessage.EntityEvents));
        }

        private static IEnumerable<EntityView> GetAllEntityViews(EntityView entityView)
        {
            return entityView.Links
                .SelectMany(link => GetAllEntityViews(link.TargetEntity))
                .Prepend(entityView);
        }

        private static EntityView CopyEntityView(EntityView entityView, bool root)
        {
            var fieldViews = root
                ? entityView.Fields
                : Enumerable.Empty<TopicMessages.FieldView>();

            var linkViews = root
                ? entityView.Links.Select(CopyLinkView)
                : Enumerable.Empty<LinkView>();

            return new EntityView(entityView.Id, entityView.EntityType, entityView.Created, entityView.LastModified,
                entityView.CreatedBy, entityView.LastModifiedBy, fieldViews, linkViews);
        }

        private static LinkView CopyLinkView(LinkView linkView)
        {
            return new LinkView(linkView.Id, linkView.LinkType, linkView.Index, linkView.LinkDirection,
                linkView.LinkEntity, CopyEntityView(linkView.TargetEntity, root: false));
        }
    }
}

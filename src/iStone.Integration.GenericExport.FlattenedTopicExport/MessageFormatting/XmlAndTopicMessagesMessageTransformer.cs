﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Xml.Linq;
using iStone.Integration.GenericExport.MessageTransformation;
using iStone.Integration.GenericExport.MessageTransformation.Xml.Standard;
using iStone.Integration.GenericExport.TopicMessages;
using iStone.Integration.GenericExport.Utilities;

namespace iStone.Integration.GenericExport.FlattenedTopicExport.MessageFormatting
{
    public class XmlAndTopicMessagesMessageTransformer : IMessageTransformer<ValueTuple<TopicMessage, XElement>>
    {
        private readonly IMessageTransformer<XElement> _transformer;

        public XmlAndTopicMessagesMessageTransformer(IMessageTransformer<XElement> transformer)
        {
            _transformer = transformer ?? throw new ArgumentNullException(nameof(transformer));
        }

        public ValueTuple<TopicMessage, XElement> TransformMessage(TopicMessage topicMessage, PimCache pimCache)
        {
            return (topicMessage, _transformer.TransformMessage(topicMessage, pimCache));
        }

        public static XmlAndTopicMessagesMessageTransformer CreateDefault(
            CultureInfo masterLanguage, IReadOnlyList<CultureInfo> exportLanguages)
        {
            var defaultTransformer = DefaultXmlMessageTransformer.Create(masterLanguage, exportLanguages);
            return new XmlAndTopicMessagesMessageTransformer(defaultTransformer);
        }
    }
}

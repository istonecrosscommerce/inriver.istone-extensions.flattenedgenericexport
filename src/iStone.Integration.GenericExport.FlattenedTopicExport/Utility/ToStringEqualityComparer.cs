﻿using System.Collections.Generic;

namespace iStone.Integration.GenericExport.FlattenedTopicExport.Utility
{
    public class ToStringEqualityComparer<T> : IEqualityComparer<T>
    {
        public bool Equals(T x, T y)
        {
            return x == null
                ? y == null
                : y != null && x.ToString() == y.ToString();
        }

        public int GetHashCode(T obj)
        {
            return obj?.ToString()?.GetHashCode() ?? 0;
        }
    }
}
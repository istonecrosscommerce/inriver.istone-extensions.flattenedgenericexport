﻿using System;
using System.Linq;
using iStone.inRiver.Core;
using iStone.inRiver.Core.Client;
using iStone.inRiver.Core.Extensions;
using iStone.inRiver.Portability.Interface;
using iStone.inRiver.Portability.Objects;
using iStone.Integration.GenericExport.Criteria;
using iStone.Integration.GenericExport.EntityEvents;
using iStone.Integration.GenericExport.MessageSending;
using iStone.Integration.GenericExport.Utilities;

namespace iStone.Integration.GenericExport.FlattenedTopicExport
{

    public class MultiMessageEntityEventExporter<TResult>
    {
        private readonly IPimApi _pimApi;
        private readonly IMap _map;
        private readonly IModelGetter _modelGetter;

        private readonly IEntityEventCreator _entityEventCreator;
        private readonly ITopicMessagesCreator _topicMessagesCreator;
        private readonly IMessageSender<TResult> _messageSender;
        private readonly IMessageSentResultHandler<TResult> _messageSentResultHandler;

        public MultiMessageEntityEventExporter(PimApi pimApi, IEntityEventCreator entityEventCreator,
            ITopicMessagesCreator topicMessagesCreator, IMessageSender<TResult> messageSender,
            IMessageSentResultHandler<TResult> messageSentResultHandler)
        {
            _pimApi = pimApi ?? throw new ArgumentNullException(nameof(pimApi));
            _map = pimApi;
            _modelGetter = pimApi.ModelGetter;

            _entityEventCreator = entityEventCreator ?? throw new ArgumentNullException(nameof(entityEventCreator));
            _topicMessagesCreator = topicMessagesCreator ?? throw new ArgumentNullException(nameof(topicMessagesCreator));
            _messageSender = messageSender ?? throw new ArgumentNullException(nameof(messageSender));
            _messageSentResultHandler = messageSentResultHandler ?? throw new ArgumentNullException(nameof(messageSentResultHandler));
        }

        public void EntityCreated(int entityId)
        {
            var entityEvent = _entityEventCreator.EntityCreated(entityId);
            HandleEntityEvent(entityEvent, new EvaluationInfo(entityEvent));
        }

        public void EntityUpdated(int entityId, string[] fields)
        {
            if (fields == null) throw new ArgumentNullException(nameof(fields));

            var entityEvent = _entityEventCreator.EntityUpdated(entityId, fields);
            var fieldTypes = fields.Select(fieldTypeId => _modelGetter.GetNonNullFieldType(fieldTypeId));
            var evaluationInfo = new EvaluationInfo(fieldTypes, entityEvent);
            HandleEntityEvent(entityEvent, evaluationInfo);
        }

        public void EntityDeleted<TRemotingEntity>(TRemotingEntity deletedEntity)
        {
            if (deletedEntity == null) throw new ArgumentNullException(nameof(deletedEntity));

            var entity = _map.Map<Entity>(deletedEntity);
            var entityEvent = _entityEventCreator.EntityDeleted(entity);
            HandleEntityEvent(entityEvent, new EvaluationInfo(entityEvent));
        }

        public void LinkCreated(int linkId, int sourceId, int targetId, string linkTypeId, int? linkEntityId)
        {
            if (linkTypeId == null) throw new ArgumentNullException(nameof(linkTypeId));

            var entityEvent = _entityEventCreator.LinkCreated(linkId, sourceId, targetId, linkTypeId, linkEntityId);
            HandleEntityEvent(entityEvent, new EvaluationInfo(entityEvent));
        }

        public void LinkUpdated(int linkId, int sourceId, int targetId, string linkTypeId, int? linkEntityId)
        {
            if (linkTypeId == null) throw new ArgumentNullException(nameof(linkTypeId));

            var entityEvent = _entityEventCreator.LinkUpdated(linkId, sourceId, targetId, linkTypeId, linkEntityId);
            HandleEntityEvent(entityEvent, new EvaluationInfo(entityEvent));
        }

        public void LinkDeleted(int linkId, int sourceId, int targetId, string linkTypeId, int? linkEntityId)
        {
            if (linkTypeId == null) throw new ArgumentNullException(nameof(linkTypeId));

            var entityEvent = _entityEventCreator.LinkDeleted(linkId, sourceId, targetId, linkTypeId, linkEntityId);
            HandleEntityEvent(entityEvent, new EvaluationInfo(entityEvent));
        }

        public void ChannelEntityCreated(int entityId, int channelId)
        {
            var entityEvent = _entityEventCreator.ChannelEntityCreated(entityId, channelId);
            HandleEntityEvent(entityEvent, new EvaluationInfo(entityEvent));
        }

        public void ChannelEntityUpdated(int entityId, string[] fields, int channelId)
        {
            if (fields == null) throw new ArgumentNullException(nameof(fields));

            var entityEvent = _entityEventCreator.ChannelEntityUpdated(entityId, fields, channelId);
            var fieldTypes = fields.Select(fieldTypeId => _modelGetter.GetNonNullFieldType(fieldTypeId));
            HandleEntityEvent(entityEvent, new EvaluationInfo(fieldTypes, entityEvent));
        }

        public void ChannelEntityDeleted(Entity deletedEntity, int channelId)
        {
            if (deletedEntity == null) throw new ArgumentNullException(nameof(deletedEntity));

            var entityEvent = _entityEventCreator.ChannelEntityDeleted(deletedEntity, channelId);
            HandleEntityEvent(entityEvent, new EvaluationInfo(entityEvent));
        }

        public void ChannelLinkCreated(int linkId, int sourceId, int targetId, string linkTypeId, int? linkEntityId, int channelId)
        {
            if (linkTypeId == null) throw new ArgumentNullException(nameof(linkTypeId));

            var entityEvent = _entityEventCreator.ChannelLinkCreated(linkId, sourceId, targetId, linkTypeId, linkEntityId, channelId);
            HandleEntityEvent(entityEvent, new EvaluationInfo(entityEvent));
        }

        public void ChannelLinkUpdated(int linkId, int sourceId, int targetId, string linkTypeId, int? linkEntityId, int channelId)
        {
            if (linkTypeId == null) throw new ArgumentNullException(nameof(linkTypeId));

            var entityEvent = _entityEventCreator.ChannelLinkUpdated(linkId, sourceId, targetId, linkTypeId, linkEntityId, channelId);
            HandleEntityEvent(entityEvent, new EvaluationInfo(entityEvent));
        }

        public void ChannelLinkDeleted(int linkId, int sourceId, int targetId, string linkTypeId, int? linkEntityId, int channelId)
        {
            if (linkTypeId == null) throw new ArgumentNullException(nameof(linkTypeId));

            var entityEvent = _entityEventCreator.ChannelLinkDeleted(linkId, sourceId, targetId, linkTypeId, linkEntityId, channelId);
            HandleEntityEvent(entityEvent, new EvaluationInfo(entityEvent));
        }

        private void HandleEntityEvent(EntityEvent entityEvent, EvaluationInfo evaluationInfo)
        {
            var pimCache = new PimCache(_pimApi);
            var topicMessages = _topicMessagesCreator.CreateTopicMessages(entityEvent, evaluationInfo, pimCache);

            TResult[] results;
            try
            {
                results = topicMessages
                    .Select(topicMessage => _messageSender.SendMessage(topicMessage, pimCache))
                    .ToArray();
            }
            catch (Exception e)
            {
                _messageSentResultHandler.HandleError(entityEvent, e);
                return;
            }

            _messageSentResultHandler.HandleResults(entityEvent, results);
        }
    }
}

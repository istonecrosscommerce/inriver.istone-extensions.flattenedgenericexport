﻿using System;
using System.Collections.Generic;
using System.Linq;
using iStone.inRiver.Core;
using iStone.inRiver.Core.Extensions;
using iStone.inRiver.Portability.Objects;
using iStone.inRiver.Portability.Query;
using iStone.Integration.GenericExport.Criteria;
using iStone.Integration.GenericExport.EntityEvents;
using iStone.Integration.GenericExport.FieldTemplates;
using iStone.Integration.GenericExport.Topics;

namespace iStone.Integration.GenericExport.FlattenedTopicExport.Tests
{
    public class SimpleEntityGraphNode
    {
        public string EntityTypeId { get; }
        public LinkDirection LinkDirection { get; }
        public IReadOnlyList<string> FieldTypeIds { get; }
        public IReadOnlyList<SimpleEntityGraphNode> Children { get; }

        public SimpleEntityGraphNode(string entityTypeId, IReadOnlyList<SimpleEntityGraphNode> children,
            LinkDirection linkDirection, IReadOnlyList<string> fieldTypeIds)
        {
            EntityTypeId = entityTypeId ?? throw new ArgumentNullException(nameof(entityTypeId));
            Children = children ?? throw new ArgumentNullException(nameof(children));
            LinkDirection = linkDirection;
            FieldTypeIds = fieldTypeIds;
        }

        public static readonly SimpleEntityGraphNode ProductNode = new SimpleEntityGraphNode(
            TestModelConstants.Product,
            new SimpleEntityGraphNode[0],
            LinkDirection.OutBound,
            new[]{TestModelConstants.ProductId});

        public static readonly SimpleEntityGraphNode ItemNode = new SimpleEntityGraphNode(
            TestModelConstants.Item,
            new SimpleEntityGraphNode[0],
            LinkDirection.OutBound,
            new string[0]);

        public static readonly SimpleEntityGraphNode ResourceNode = new SimpleEntityGraphNode(
            TestModelConstants.Resource,
            new SimpleEntityGraphNode[0],
            LinkDirection.OutBound,
            new string[0]);

        public static readonly SimpleEntityGraphNode ResourceWithFileIdNode = new SimpleEntityGraphNode(
            TestModelConstants.Resource,
            new SimpleEntityGraphNode[0],
            LinkDirection.OutBound,
            new[] {TestModelConstants.ResourceFileId});

        public static readonly SimpleEntityGraphNode ResourceWithFilenameNode = new SimpleEntityGraphNode(
            TestModelConstants.Resource,
            new SimpleEntityGraphNode[0],
            LinkDirection.OutBound,
            new[] {TestModelConstants.ResourceFilename});

        public static readonly SimpleEntityGraphNode ResourceWithFileIdAndNameNode = new SimpleEntityGraphNode(
            TestModelConstants.Resource,
            new SimpleEntityGraphNode[0],
            LinkDirection.OutBound,
            new[] {TestModelConstants.ResourceFileId, TestModelConstants.ResourceFilename});

        public static readonly SimpleEntityGraphNode ChannelNodeNode = new SimpleEntityGraphNode(
            TestModelConstants.ChannelNode,
            new SimpleEntityGraphNode[0],
            LinkDirection.InBound,
            new string[0]);

        public static readonly SimpleEntityGraphNode ProductItemNode = new SimpleEntityGraphNode(
            TestModelConstants.Product,
            new[]
            {
                ItemNode
            },
            LinkDirection.OutBound,
            new[] { TestModelConstants.ProductId });

        public static readonly SimpleEntityGraphNode ItemResourceNode = new SimpleEntityGraphNode(
            TestModelConstants.Item,
            new[]
            {
                ResourceNode
            },
            LinkDirection.OutBound,
            new string[0]);

        public static readonly SimpleEntityGraphNode ProductItemResourceNode = new SimpleEntityGraphNode(
            TestModelConstants.Product,
            new[]
            {
                ItemResourceNode,
                ResourceNode
            },
            LinkDirection.OutBound,
            new[] { TestModelConstants.ProductId });

        public static readonly SimpleEntityGraphNode FlatProductItemResourceNode = new SimpleEntityGraphNode(
            TestModelConstants.Product,
            new[]
            {
                ItemNode,
                ResourceNode
            },
            LinkDirection.OutBound,
            new[] { TestModelConstants.ProductId });

        public static readonly SimpleEntityGraphNode ProductItemResourceChannelNodeNode = new SimpleEntityGraphNode(
            TestModelConstants.Product,
            new[]
            {
                ItemResourceNode,
                ResourceNode,
                ChannelNodeNode
            },
            LinkDirection.OutBound,
            new[] { TestModelConstants.ProductId });

        public static readonly SimpleEntityGraphNode FlatProductItemResourceChannelNodeNode =
            new SimpleEntityGraphNode(
                TestModelConstants.Product,
                new[]
                {
                    ItemNode,
                    ResourceNode,
                    ChannelNodeNode
                },
                LinkDirection.OutBound,
                new[] { TestModelConstants.ProductId });

        public static readonly SimpleEntityGraphNode ProductResourceWithFieldTypesNode = new SimpleEntityGraphNode(
            TestModelConstants.Product,
            new[]
            {
                ResourceNode,
                ResourceWithFileIdNode,
                ResourceWithFilenameNode
            },
            LinkDirection.OutBound,
            new[] { TestModelConstants.ProductId });

        public static readonly SimpleEntityGraphNode ProductResourceNode = new SimpleEntityGraphNode(
            TestModelConstants.Product,
            new[]
            {
                ResourceNode
            },
            LinkDirection.OutBound,
            new[] { TestModelConstants.ProductId });

        public static readonly SimpleEntityGraphNode ProductResourceWithFieldTypesMergedNode = new SimpleEntityGraphNode(
            TestModelConstants.Product,
            new[]
            {
                ResourceWithFileIdAndNameNode
            },
            LinkDirection.OutBound,
            new[] { TestModelConstants.ProductId });

        public static Topic CreateTopic(IPimApi pimApi, SimpleEntityGraphNode simpleRoot)
        {
            var root = CreateEntityTypeGraphNode(pimApi, simpleRoot);

            return new Topic("TestTopic", "TestTopic Description", new Dictionary<string, string>(),
                root, EntityEventTypeUtility.AllEventTypes,
                new Dictionary<EntityEventType, IEnumerable<EntityType>>(),
                new Dictionary<EntityEventType, IEnumerable<FieldType>>(),
                new Dictionary<EntityEventType, IEnumerable<LinkType>>());
        }

        public static EntityTypeGraphNode CreateEntityTypeGraphNode(IPimApi pimApi, SimpleEntityGraphNode simpleNode)
        {
            var entityType = pimApi.ModelGetter.GetNonNullEntityType(simpleNode.EntityTypeId);
            var fieldTypes = entityType.FieldTypes.Where(fieldType => simpleNode.FieldTypeIds.Contains(fieldType.Id));

            var criteriaFactory = new CriteriaFactory(pimApi.ModelGetter, pimApi.GetMasterLanguage());

            return new EntityTypeGraphNode(entityType, criteriaFactory.CreateAlwaysTrueCriteria(),
                fieldTypes, Enumerable.Empty<IFieldViewTemplate>(),
                simpleNode.Children.SelectMany(childNode => CreateEntityTypeGraphNodeLinks(pimApi, simpleNode, childNode)));
        }

        public static List<EntityTypeGraphNodeLink> CreateEntityTypeGraphNodeLinks(IPimApi pimApi,
            SimpleEntityGraphNode sourceNode, SimpleEntityGraphNode targetNode)
        {
            return pimApi.ModelService.GetLinkTypesForEntityType(sourceNode.EntityTypeId)
                .Where(linkType => (targetNode.LinkDirection == LinkDirection.OutBound &&
                                    linkType.SourceEntityTypeId == sourceNode.EntityTypeId &&
                                    linkType.TargetEntityTypeId == targetNode.EntityTypeId) ||
                                   (targetNode.LinkDirection == LinkDirection.InBound &&
                                    linkType.TargetEntityTypeId == sourceNode.EntityTypeId &&
                                    linkType.SourceEntityTypeId == targetNode.EntityTypeId))
                .Select(linkType => new EntityTypeGraphNodeLink(CreateEntityTypeGraphNode(pimApi, targetNode),
                    linkType, targetNode.LinkDirection, null))
                .ToList();
        }
    }
}
